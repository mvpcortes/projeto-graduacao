using System;
using System.Collections.Generic;
using System.Text;

namespace MotivationalSystem
{
    public static class XMLDefines
    {
        public const string CLASS_TAG           = "class";
        public const string EXTEND_TAG          = "extends";

        public const string MOT_SYSTEM_TAG      = "motivationalsystem";
        public const string EMOTION_TAG         = "emotion";
        public const string SENSOR_TAG          = "sensor";
        public const string DRIVE_TAG           = "drive";
        public const string BEHAVIOR_TAG        = "behavior";
        public const string APPETITIVE_TAG      = "appetitive";

        public const string INFLUENCE_TAG       = "influence";
        public const string CONSUMMATE_INF_TAG = "consummate_influence";

        public const string TYPE_TAG            = "type";
        public const string NAME_TAG            = "name";
        public const string OPERATION_TAG       = "operation";
        public const string WEIGHT_TAG          = "weight";
        public const string CONSUMMATORY_TAG    = "consummatory";
        public const string ENERGY_TAG          = "energy_drive";

        public const string GOOD_TAG            = "good";
        public const string POLE_TAG            = "pole";
        public const string SPEND_TAG           = "spend";

        public const string OVER_STATE_TAG      = "over_state";
        public const string UNDER_STATE_TAG     = "under_state";
        public const string EVERY_STATE_TAG     = "every_state";

        public const string INC_RATE_TAG = "incRate";
        public const string DEC_RATE_TAG = "decRate";


        public const string THESHOLD_TAG = "threshold";
        public const string THESHOLD_OVER_TAG = "over";
        public const string THESHOLD_UNDER_TAG = "under";

        public const string AVATAR              = "avatar";
        public const string AVATAR_COLOR        = "color";
        public const string NEUTRAL_AVATAR      = "neutral_avatar";
    }
}
