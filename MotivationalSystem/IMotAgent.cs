﻿using System;
using System.Drawing;
namespace MotivationalSystem
{
    //Interface do controlador motivacional
    public interface IMotAgent
    {
        /// <summary>
        /// Retorna um container contendo os processos do controle
        /// </summary>
        System.Collections.Generic.ICollection<IProcessor> ContentProcessor 
        { get; }

        /// <summary>
        /// Limpa o buffer dos sensores. Isto é, apaga todos os perceptions adicionados na última atualização
        /// </summary>
        //void clearSensorBuffer();

        /// <summary>
        /// Atribui um action a um ID de comportamento.
        /// </summary>
        /// <remarks>
        /// Atribui uma ação a um ID de comportamento. Quando um comportamento for ativado, ele tentará executar a ação associada ao IAction que foi setado aqui.
        /// </remarks>
        /// <param name="id">id do comportamento</param>
        /// <param name="action">ação a ser executada</param>
        void setAction(string id, IAction action);

        /// <summary>
        /// Obtém um sensor para permitir envio de perceptions
        /// </summary>
        /// <param name="id">O nome do sensor</param>
        /// <returns>O Sensor</returns>
        ISensor getSensor(string id);


        /// <summary>
        /// Atualiza o sistema motivacional com 1 passo de tempo
        /// </summary>
        /// <param name="actualTime">Tempo atual</param>
        /// <param name="deltaTime">tempo decorrido desde a última atualização</param>
        void update(TimeSpan actualTime, TimeSpan deltaTime);


        /// <summary>
        /// Retorna a cor da emoção corrente. Caso não haja emoção corrente retorna a cor branca (neutra)
        /// </summary>
        /// <returns></returns>
        float[] getEmotionalColor();

        /// <summary>
        /// Retorna a emoção corrente (o nome dela).
        /// </summary>
        /// <returns></returns>
        string getVisibleEmotion();

        /// <summary>
        /// Retorna uma mistura de todas as cores das emoções ativas.
        /// </summary>
        /// <returns></returns>
        float[] getBlendEmotionalColor();


        /// <summary>
        /// incrementa ou decrementa o valor dos processos usando um raio de (-range, +range). Bom ser usado no início do sistema
        /// </summary>
        /// <param name="range">valor a ser usado no raio.</param>
        void randomizeProcessorsValues(float range);

        /// <summary>
        /// Retorna o valor de badBeing (mal-estar) do agente. Usado como medida.
        /// </summary>
        /// <returns></returns>
        float badBeing();

        /// <summary>
        /// Retorna um valor de wellBeing (bem-estar) do agente. Usado como medida.
        /// <returns></returns>
        float wellBeing();

    }
}
