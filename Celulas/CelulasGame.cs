using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using edde;
using edde.maps;
using edde.components;

namespace Celulas
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class CelulasGame : edde.RPG
    {
        private const int CELULA_COUNT = 5;
        private Map mMap;
        private FPS mFPS;

        private int mCellCreateCount;

        private GameObject mGOText = new GameObject();
        private TextComponent mSCText = new TextComponent("arial_12_b");


        public static int cellCreateCount
        {
            get { return ((CelulasGame)RPG.me).mCellCreateCount++; }
        }

        List<Celula> mListCelula = new List<Celula>();

        public CelulasGame()
        {
            mFPS = new edde.FPS(this);
            Components.Add(mFPS);
        }


        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            mMap = new edde.maps.Map(this, "mapa");

            mMap.floorLayer = new edde.maps.PanoramaLayer(@"background\background", Point.Zero);
            Random random = new Random();

            {//Cria c�lulas
                Vector2 pos = new Vector2(200, 200);
                mListCelula.Capacity = CELULA_COUNT;
                for (int i = 0; i < CELULA_COUNT; i++)
                {
                    mListCelula.Add(new Celula(string.Format("Cell {0}", cellCreateCount), new GeneCelula()));
                    mListCelula[i].position = pos;//(MyMath.getRandomPos((float)screenWidth - 20f, (float)screenHeight - 20f) + (new Vector2(10, 10)));
                    mMap.addObject(mListCelula[i]);
                    pos += Vector2.One * 20;
                }
            }

            //
            mGOText.addComponent(mSCText);
            mGOText.position = new Vector2(0, 0);
            mSCText.color = Color.Green;
            mMap.addObject(mGOText);
            //

            mMap.setSize(screenWidth, screenHeight);
            sceneManager.enterScene(mMap);
            mMap.init();

            mFPS.DrawOrder = 4;
            sceneManager.DrawOrder = 1;


        }

        protected override void Initialize()
        {
            base.Initialize();
            IsFixedTimeStep = true;

        }

        private void realPoda()
        {
            List<Celula> list = new List<Celula>();

            this.sceneManager.topScene.getGameObjects<Celula>(list);
            if (list.Count >= 200)
            {
                int count = list.Count;
                for (int i = 0; i < count - 1; ++i)
                {
                    //Celula g = (Celula)arr.GetValue(i);
                    for (int j = i + 1; j < count; ++j)
                    {
                        //Celula h = (Celula)arr.GetValue(j);
                        if (list[i].gene.getOptimalValue() > list[j].gene.getOptimalValue())
                        {
                            Celula temp = list[i];
                            list[i] = list[j];
                            list[j] = temp;
                        }
                    }
                }

                for (int i = 0; i < list.Count / 2; i++)
                {
                    sceneManager.removeSafeGameObject(list[i]);
                    ReputationSystem.ReputationCenter.Instance.removeAgent(list[i]);
                }
            }
        }

        private void randomPoda()
        {
             List<Celula> list = new List<Celula>();

            this.sceneManager.topScene.getGameObjects<Celula>(list);
            if (list.Count >= 200)
            {
                List<float> listWeight = new List<float>(list.Count);
                for (int i = 0; i < list.Count; i++)
                    listWeight.Add(MyMath.getRandomFloat(1));

                for (int i = 0; i < list.Count; i++)
                {
                    int minPos = i;
                    for (int j = i + 1; j < list.Count; j++)
                    {
                        if (listWeight[minPos] > listWeight[j])
                        {
                            minPos = j;
                        }
                    }

                    if(minPos > i)
                    {
                        Celula cellTemp = list[i];
                        list[i] = list[minPos];
                        list[minPos] = cellTemp;

                        float temp = listWeight[i];
                        listWeight[i] = listWeight[minPos];
                        listWeight[minPos] = temp;
                    }
                }
                for (int i = 0; i < list.Count / 2; i++)
                {
                    sceneManager.removeSafeGameObject(list[i]);
                    ReputationSystem.ReputationCenter.Instance.removeAgent(list[i]);
                }

            }
        }
        private void reputationPoda()
        {
            List<Celula> list = new List<Celula>();
            List<float> listRep = new List<float>();
            this.sceneManager.topScene.getGameObjects<Celula>(list);

            //float media = 0;

            //for (int i = 0; i < list.Count; i++)
            //{
            //    float repA = 0f;
            //    if (!ReputationSystem.ReputationCenter.Instance.getReputation(list[i], out repA))
            //        repA = 0f;// float.MaxValue;
            //    media += (media + repA) / 2f;
            //}

            if (list.Count >= 200)
            {
                for(int i = 0; i < list.Count; i++)
                {
                    for (int j = i; j < list.Count; j++)
                    {
                        float repA = 0f;
                        if (!ReputationSystem.ReputationCenter.Instance.getReputation(list[i], out repA))
                            repA = 0;// float.MaxValue;

                        float repB = 0f;
                        if (!ReputationSystem.ReputationCenter.Instance.getReputation(list[j], out repB))
                            repB = 0;// float.MaxValue;

                        if (repA > repB)
                        {
                            Celula temp = list[i];
                            list[i] = list[j];
                            list[j] = temp;
                        }
                    }

                    {
                        float repA = 0f;
                        if (!ReputationSystem.ReputationCenter.Instance.getReputation(list[i], out repA))
                            repA = 0;

                        listRep.Add(repA);
                    }
                }

                for (int i = 0; i < list.Count / 2; i++)
                {
                    sceneManager.removeSafeGameObject(list[i]);
                    ReputationSystem.ReputationCenter.Instance.removeAgent(list[i]);
                }
            }
        }

        protected override void Update(GameTime time)
        {
             
            int count = this.sceneManager.topScene.getGameObjects().Count - 1;
            mSCText.text = (count).ToString();
            base.Update(time);

            if (count > 200) //poda
            {
                reputationPoda();
                //realPoda();
                //randomPoda();
            }

            ////faz o corte, removendo metade das c�lulas
            //if (this.inputControl.isKeyDown(InputControl.RPG_KEY.ACTION_A))
            //{
            //    List<Celula> list = new List<Celula>();
            //    sceneManager.topScene.getGameObjects<Celula>(list);

            //    list.Sort(new ReputationSystem.ReputationCenter.ComparableReputable<Celula>(false));

            //    for (int i = list.Count / 2; i < list.Count; i++)
            //    {
            //        this.sceneManager.removeSafeGameObject(list[i]);
            //    }
            //}
        }
    }
}
