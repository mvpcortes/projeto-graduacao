﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde.physicalmanager;

namespace ReputationSystem
{
    public interface IReputable: IComparable<IReputable>, ICollised
    {
        string name
        {
            get;
            set;
        }
    }
}
