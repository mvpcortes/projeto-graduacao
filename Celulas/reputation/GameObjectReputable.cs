﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReputationSystem;
using edde;

namespace Celulas.reputation
{
    class GameObjectReputable : GameObject, IReputable, IComparable<GameObjectReputable>
    {
        private string mName;
        #region IReputable Members

        public string name
        {
            get { return mName; }
            set { mName = value; }
        }

        #endregion

        #region IComparable<IReputable> Members

        public int CompareTo(IReputable other)
        {
            return mName.CompareTo(other.name);
        }

        #endregion

        #region IComparable<GameObjectReputable> Members

        public int CompareTo(GameObjectReputable other)
        {
            return CompareTo((IReputable)other);
        }

        #endregion
    }
      
}
