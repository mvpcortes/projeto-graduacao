﻿
using edde;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using edde.physicalmanager;
using Celulas.reputation;
using ReputationSystem;
using Celulas;
class FindAndFollowComponent : SoftComponent
{
    /// <summary>
    /// Indivíduo sendo seguido
    /// </summary>
    private Celula mFollowIt;

    private const float RANGE_VISION = 400;
    /// <summary>
    /// GameOBject pai
    /// </summary>
    private GameObject mParent;

    /// <summary>
    /// Incremento da posição;
    /// </summary>
    private float mVelocity;

    private bool mEnabled = false;

    private Scene mParentScene = null;

    private List<ICollised> mListObjectRange = new List<ICollised>();

    #region properties
    public Celula followIt
    {
        get{return mFollowIt;}
        
        set{

            mFollowIt = value;
        }
    }

    private GameObject parent
    {
        get{return mParent;}
    }


    public Scene parentScene
    {
        get { return mParentScene; }
        set { mParentScene = value; }
    }
    
    public float velocity
    {
        get { return mVelocity; }
        set { mVelocity = value; }
    }
    #endregion


    private void findNextFollow()
    {
        mListObjectRange.Clear();
        followIt = null;
        parentScene.getGameObjectInRegion(parent.position, RANGE_VISION, mListObjectRange);
        
        util.PriorityList<IReputable> priorityList = new util.PriorityList<IReputable>
                (new ReputationSystem.ReputationCenter.ComparableReputable<IReputable>(false));
        foreach (ICollised collised in mListObjectRange)
        {
            if (collised is IReputable)
                priorityList.Add(collised as IReputable);
        }

            
        Celula follow = null;
        foreach (IReputable reputable in priorityList)
        {
            if (!ReferenceEquals(reputable, parent))
            {
                follow = reputable as Celula;
                if(
                    !ReferenceEquals(follow, null) &&
                    (follow.state == Celulas.components.CelulaController.STATE.FOLLOW)
                  )
                    break;
            }

        }

        if(!ReferenceEquals(follow, null))
            followIt = follow;
    }

    #region SoftComponent Members
    public void draw(Microsoft.Xna.Framework.GameTime time, ref Microsoft.Xna.Framework.Rectangle rectWindow)
    {
       //faz nada
        return;
    }

    public void update(Microsoft.Xna.Framework.GameTime time)
    {
        if (ReferenceEquals(followIt, null))
        {
            findNextFollow();
            if (ReferenceEquals(this.followIt, null))
            {
                enable = false;
            }
        }
        else if (followIt.ejected)//caso seja injetado, tenta outro
        {
            followIt = null;
        }
        else
        {
            Vector2 vec = followIt.position - parent.position;
            vec.Normalize();
            parent.setDirection(vec);
            parent.incPosition(vec * velocity);
        }
    }

    public bool init(GameObject parent)
    {
        mParent = parent;

        return true;
    }

    public void ejected()
    {
        
    }

    public bool ejectMe()
    {
        return false;
    }

    public bool enable
    {
        get { return mEnabled; }
        set 
        {
            mEnabled = value;
            if (mEnabled == false)
                followIt = null;
        }
    }

    #endregion
}