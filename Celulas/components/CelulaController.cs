﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using edde.components;
using Celulas.reputation;
using ReputationSystem;

namespace Celulas.components
{
    /// <summary>
    /// Faz o controle da máquina de estados da célula
    /// </summary>
    class CelulaController: SoftComponent
    {
        #region injections
        [Injection]
        FindAndFollowComponent mFindFollow = null;

        [Injection]
        RandomMoviment mRandomMove = null;

        [Injection]
        CoupleComponent mCouple = null;

        #endregion
        
        public enum STATE
        {
            RANDOM = 1,
            FOLLOW = 2,
            COUPLE = 3
        }

        private STATE mState = STATE.RANDOM;

        /// <summary>
        /// Tempo de espera para entrar no estado de procura
        /// </summary>
        private TimeSpan mDelayTimeFollow = TimeSpan.FromSeconds(3f);

        /// <summary>
        /// último tempo que ele começou a andar randomico
        /// </summary>
        private TimeSpan mLastTimeChangeMachine = TimeSpan.Zero;

        /// <summary>
        /// Tempo de cópula
        /// </summary>
        private readonly TimeSpan DELAY_COUPLE = TimeSpan.FromSeconds(3f);

        /// <summary>
        /// Tempo de vida da célula
        /// </summary>
        private TimeSpan mTimeLife = TimeSpan.FromSeconds(30);

        /// <summary>
        /// A data (tempo) de nascimento
        /// </summary>
        private TimeSpan mTimeBorn = TimeSpan.Zero;

        /// <summary>
        /// Tempo a ser esperado para começar a seguir outros atrás de acasalamento
        /// </summary>
        private TimeSpan mDelayForFollow = TimeSpan.FromSeconds(3f);

        /// <summary>
        /// Referência a o pai
        /// </summary>
        private Celula mParent;
        
        #region properties
        public STATE state
        {
            get { return mState; }
            set 
            {
                mState = value;
                mRandomMove.enable = false;
                mFindFollow.enable = false;
                mCouple.enable = false;
                switch (state)
                {
                    case STATE.RANDOM:
                        mRandomMove.enable = true;
                        break;

                    case STATE.FOLLOW:
                        mFindFollow.enable = true;
                        break;
                    case STATE.COUPLE:
                        mCouple.enable = true;
                        break;
                }
            }
        }

        public static string toString(STATE _state)
        {
            switch (_state)
            {
                case STATE.RANDOM:
                    return "R";
                case STATE.FOLLOW:
                    return "F";
                case STATE.COUPLE:
                    return "C";

                default:
                    return "";
            }

        }

        public TimeSpan delayForFollow
        {
            get { return mDelayForFollow; }
            set { mDelayForFollow = value; }
        }

        /// <summary>
        /// Retorna o tempo de vida da célula (delta
        /// </summary>
        public TimeSpan timeLife
        {
            get { return mTimeLife; }
            set { mTimeLife = value; }
        }

        /// <summary>
        /// Get o tempo (data) de nascimento
        /// </summary>
        public TimeSpan timeBorn
        {
            get { return mTimeBorn; }
        }

        /// <summary>
        /// Retorna o parent
        /// </summary>
        public Celula parent
        {
            get { return mParent; }

        }

        public Celula otherTouch
        {
            get{return mCouple.otherTouch;}
            set { mCouple.otherTouch = value as Celula; }
        }

        #endregion

        #region SoftComponent Members

        public void draw(Microsoft.Xna.Framework.GameTime time, ref Microsoft.Xna.Framework.Rectangle rectWindow)
        {
            //faznada
        }

        public void update(Microsoft.Xna.Framework.GameTime time)
        {
            if (mTimeBorn == TimeSpan.Zero)
            {
                mLastTimeChangeMachine = mTimeBorn = time.TotalGameTime;
                
            }

            else if(time.TotalGameTime - timeBorn  > timeLife)//remove o cara do mapa
            {
                RPG.me.sceneManager.removeSafeGameObject(parent);
                parent.ejected = true;
                ReputationSystem.ReputationCenter.Instance.removeAgent(parent);

                return;
            }

            STATE prevState = state;
            TimeSpan actualTime = time.TotalGameTime;
            switch (state)
            {
                case STATE.RANDOM:
                    if (actualTime - mLastTimeChangeMachine > delayForFollow)
                        state = STATE.FOLLOW;
                break;

                case STATE.FOLLOW:
                if (mFindFollow.enable == false)//caso não esteja mais operando pq não achou
                {
                    state = STATE.RANDOM;
                }

                if (!ReferenceEquals(otherTouch, null))//faz acasalamento
                {
                    if (RPG.me.sceneManager.topScene.containsGameObject(otherTouch))
                        state = STATE.COUPLE;
                    else
                    {
                        mFindFollow.followIt = null;
                        otherTouch = null;
                    }
                }
                break;
                case STATE.COUPLE:
                

                if ( mCouple.enable == false || (actualTime - mLastTimeChangeMachine > DELAY_COUPLE))
                {
                    state = STATE.RANDOM;
                }
                break;
            }

            if (state != prevState)
                mLastTimeChangeMachine = actualTime;
        }

        public bool init(GameObject _parent)
        {
            mParent = _parent as Celula;
            return !ReferenceEquals(mParent, null);
        }

        public void ejected()
        {
        }

        public bool ejectMe()
        {
            return false;
        }

        public bool enable
        {
            get { return true; }
        }

        #endregion
    }
}
