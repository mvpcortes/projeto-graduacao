﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using edde.components;

namespace Detetive
{
    class Lapide: GameObject
    {
        [Injection()]
        CharSetComponent mCharSet;

        public Lapide(Npc npc)
        {
            mCharSet = new CharSetComponent("shot", CharSet.PATTERN.PATTERN_FIVE_FRAME_LOOP);
            mCharSet.setAnimatedState(CharSetComponent.ANIMATED_STATE.FIX);
            mCharSet.setPointCharSet(0, 1);
            mCharSet.selfDirection = MyMath.VEC_LEFT;
            mCharSet.setRelativeFrame(1);

            //
            setPosition(npc.position);
            RPG.me.sceneManager.insertSafeGameObject(this);
        }
    }
}
