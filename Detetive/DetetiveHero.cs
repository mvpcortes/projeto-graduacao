﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using edde.components.motsystem;
using edde.components;
using edde.physicalmanager;
using edd.motsystem;

namespace Detetive
{
    public class DetetiveHero: GameObject
    {
        #region Injection
        [Injection]
        private CharSetComponent mCharSet;

        [Injection]
        TileAnimationComponent mCharSetBallon;

        [Injection]
        private ControlComponent mControl = new ControlComponent();

        [Injection]
        private Arma mArma = new Arma();

        #endregion

        private Npc mNpcProximo = null;

        private TimeSpan mTimeLastPresente = TimeSpan.Zero;

        public DetetiveHero()
            : this(CharSet.PATTERN.PATTERN_DEFAULT)
        { }
        public DetetiveHero(CharSet.PATTERN pattern)
            : base()
        {
            mCharSet = new CharSetComponent("amanda", pattern);
            mCharSetBallon = new TileAnimationComponent("ballon");
        }

        public override void onTouch(ICollection<edde.physicalmanager.ICollised> collObject)
        {
            GameObject findNpc = null;
            foreach (ICollised collised in collObject)
            {
                GameObject actualNpc = collised as Npc;

                if (ReferenceEquals(actualNpc, null))
                    actualNpc = collised as Bandido;

                if(ReferenceEquals(actualNpc, null)) continue;

                if (ReferenceEquals(findNpc, null))
                    findNpc = actualNpc;
                else
                {
                    if ((findNpc.position - position).Length() > (actualNpc.position - position).Length())
                        findNpc = actualNpc;
                }
            }

            mNpcProximo = findNpc as Npc;
        }

        public override void update(Microsoft.Xna.Framework.GameTime time)
        {
            base.update(time);
            if (!ReferenceEquals(mNpcProximo, null))
            {
                if (RPG.me.inputControl.isKeyDown(InputControl.RPG_KEY.ACTION_C))
                {
                    if (mTimeLastPresente == TimeSpan.Zero)
                    {
                        mNpcProximo.sendPresente(new Presente(this, 1, this));
                        mTimeLastPresente = time.TotalGameTime;
                        mCharSetBallon.actualTile = Ballon.PRESENTE;
                    }
                }
            }

            if (time.TotalGameTime - mTimeLastPresente > TimeSpan.FromSeconds(2))
            {
                mCharSetBallon.actualTile = Ballon.INVALID;
                mTimeLastPresente = TimeSpan.Zero;
            }
        }

        #region IReputable Members

        public string state
        {
            get { return ""; }
        }

        public string name
        {
            get { return "hero"; }
        }

        #endregion

        #region IComparable<IReputable> Members

        #endregion

        public override int GetHashCode()
        {
            return name.GetHashCode();
        }
    }
}
