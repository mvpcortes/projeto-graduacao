﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Detetive
{
    class SomTiro: GameObject
    {
        /// <summary>
        /// Tempo inicial de criação
        /// </summary>
        TimeSpan mTimeInit = TimeSpan.Zero;

        Texture2D mTextureRange = null;

        PrimitiveBatch mPrimitiveBatch;

        
        /// <summary>
        /// Tempo exigido para eliminar o Som
        /// </summary>
        TimeSpan mTimeDelay = TimeSpan.FromSeconds(5);

        /// <summary>
        /// construtor
        /// </summary>
        /// <param name="parent">Referência ao Shot que gerou o som.</param>
        public SomTiro(Shot parent)
        {
            mPrimitiveBatch = new PrimitiveBatch(RPG.me.GraphicsDevice);

            setTypeCollision(edde.physicalmanager.COLLISION_TYPE.CLOUD);
            setPosition(parent.position);
            setRectBase(RectFloat.FromCenter(Vector2.Zero, 500));
        }

        /// <summary>
        /// Inicia o GameObject
        /// </summary>
        /// <param name="scene"></param>
        /// <returns></returns>
        public override bool init(Scene scene)
        {
            mTextureRange = Resource.getTexture2DFirstColorTransparent(@"other\blank");
            return base.init(scene);
        }

        /// <summary>
        /// Update do sistema
        /// </summary>
        /// <param name="time"></param>
        public override void update(Microsoft.Xna.Framework.GameTime time)
        {
            TimeSpan timeActual = time.TotalGameTime;
            if (mTimeInit == TimeSpan.Zero)
                mTimeInit = timeActual;
            else
            {
                if ((mTimeInit + mTimeDelay) < timeActual)
                {
                    RPG.me.sceneManager.removeSafeGameObject(this);
                }
            }
        }


        /// <summary>
        /// Pinta a imagem do raio do som
        /// </summary>
        /// <param name="time"></param>
        /// <param name="rectWindow"></param>
        public override void draw(Microsoft.Xna.Framework.GameTime time, ref Microsoft.Xna.Framework.Rectangle rectWindow)
        {
            SpriteBatch sb = RPG.me.spriteBatch;

//          RectFloat rectDest = new RectFloat((mParent.position - new Vector2(centerPoint.X, centerPoint.Y)), new Vector2(getTileWidth(), getTileHeight()));
           // RectFloat rect = getRectBase();
            RectFloat rect = new RectFloat(position - getSizeBase() / 2f, getSizeBase());

            rect.left -= rectWindow.X;
            rect.top  -= rectWindow.Y;

            //Rectangle rect = getRectBase().toRectangle();
            //rect.X += -(int)this.position.X + rectWindow.X;
            //rect.Y += -(int)this.position.Y + rectWindow.Y;

            // sb.Draw(mTextureRange, position, null, Color.Green, 0, new Vector2(mTextureRange.Width / 2f, mTextureRange.Height / 2f), 1f, SpriteEffects.None, 0);
            sb.Draw(mTextureRange, rect.toRectangle(), new Color(Color.Green, 50));

            Vector2 pos = position;
            pos.X -= rectWindow.X;
            pos.Y -= rectWindow.Y;
            RPG.me.spriteBatch.End();
            mPrimitiveBatch.Begin(PrimitiveType.LineList);
            mPrimitiveBatch.AddVertex(new Vector2(rect.left, rect.top), Color.White);
            mPrimitiveBatch.AddVertex(new Vector2(rect.left, rect.bottom), Color.White);

            mPrimitiveBatch.AddVertex(new Vector2(rect.left, rect.top), Color.White);
            mPrimitiveBatch.AddVertex(new Vector2(rect.right, rect.top), Color.White);

            mPrimitiveBatch.AddVertex(new Vector2(rect.right, rect.top), Color.White);
            mPrimitiveBatch.AddVertex(new Vector2(rect.right, rect.bottom), Color.White);

            mPrimitiveBatch.AddVertex(new Vector2(rect.left, rect.bottom), Color.White);
            mPrimitiveBatch.AddVertex(new Vector2(rect.right, rect.bottom), Color.White);
            mPrimitiveBatch.End();
            RPG.me.spriteBatch.Begin();
        }
    }
}
