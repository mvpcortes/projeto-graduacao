﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MotivationalSystem;

namespace Detetive.action
{
    /// <summary>
    /// Componente responsável por armazenar a reputação de outros agentes.
    /// Usando reputação descentralizada direta.
    /// Ele é um SoftComponent para que participe do sistema de injeção
    /// </summary>
    //class ReputationComponent: edde.SoftComponent, IComparer<IPerception>, IComparer<IReputable>
    //{
    //    private struct WapperComparation: IComparable<WapperComparation>
    //    {
    //        private IReputable mAgent;

    //        public override int  GetHashCode()
    //        {
    //             return base.GetHashCode();
    //        }

    //        public IReputable npc
    //        {
    //            get { return mAgent; }
    //        }

    //        public WapperComparation(IReputable npc)
    //        {
    //            mAgent = npc;
    //        }

    //        #region IComparable<WapperComparation> Members

    //        public int CompareTo(WapperComparation other)
    //        {
    //            return mAgent.GetHashCode() - other.mAgent.GetHashCode();
    //        }

    //        #endregion
    //    }

    //    private const float PREV_VALUE_RATE = 0.3f;
    //    private const float NEW_VALUE_RATE = 0.7f;
        
    //    private Dictionary<WapperComparation, float> mDicAgentReputation = new Dictionary<WapperComparation, float>();

    //    private Dictionary<string, float> mDicStateReputation = new Dictionary<string, float>();

    //    public ReputationComponent()
    //    {
    //    }

    //    #region container methods
    //    /// <summary>
    //    /// Adiciona uma reputação a um agente em um estado
    //    /// </summary>
    //    /// <param name="go">O agente a receber a reputação</param>
    //    /// <param name="state">O estado do agente</param>
    //    /// <param name="reputation">A reputação</param>
    //    public void addReputation(IReputable go, string state, float reputation)
    //    {
    //        if (ReferenceEquals(go, null))
    //            return;

    //        WapperComparation comp = new WapperComparation(go);

    //        float prevComp = 0;
    //        if(mDicAgentReputation.TryGetValue(comp, out prevComp))
    //        {
    //            mDicAgentReputation[comp] = PREV_VALUE_RATE*prevComp + NEW_VALUE_RATE*reputation;
    //        }else
    //        {
    //            mDicAgentReputation[comp] = reputation;
    //        }

    //        if (mDicStateReputation.TryGetValue(state, out prevComp))
    //        {
    //            mDicStateReputation[state] = PREV_VALUE_RATE * prevComp + NEW_VALUE_RATE * reputation;
    //        }
    //        else
    //        {
    //            mDicStateReputation[state] = reputation;
    //        }

    //        if (mDicAgentReputation.Count > 2)
    //        {
    //            int i = 0;
    //        }
    //    }

    //    /// <summary>
    //    /// Retorna a reputação de um Agente
    //    /// </summary>
    //    /// <param name="go">O agente a receber a reputação</param>
    //    /// <returns>O valor da reputação</returns>
    //    public bool getReputation(IReputable go, out float reputation)
    //    {
    //        WapperComparation comp = new WapperComparation(go);
    //        if ((mDicAgentReputation.TryGetValue(comp, out reputation)))
    //            return true;
    //        else
    //        {
    //            string state = go.state;
    //            return getReputation(state, out reputation);
    //        }

    //    }

    //    /// <summary>
    //    /// Retorna a reputação de um estado de agente
    //    /// </summary>
    //    /// <param name="state">Um estado possível de um agente</param>
    //    /// <returns>A reputação deste esdado</returns>
    //    public bool getReputation(string state, out float reputation)
    //    {
    //        if (mDicStateReputation.TryGetValue(state, out reputation))
    //        {
    //            return true;
    //        }
    //        else
    //        {
    //            if (m_bCrescenteOrder)
    //                reputation = float.MaxValue;
    //            else
    //                reputation = float.MinValue;
    //            return false;
    //        }

    //    }



    //    /// <summary>
    //    /// Busca dentro de um container de Agentes qual o agente com maior reputação
    //    /// </summary>
    //    /// <param name="objects">Um vetor de agentes</param>
    //    /// <param name="states">Um vetor de States. Caso nenhum agente exista dentro da base de dados, ele usa o estado deles para determinar o com maior reputação</param>
    //    /// <returns>Um enumarator apontando pro item selecionado</returns>
    //    public IEnumerator<IReputable> getTheHighReputation(ICollection<IReputable> coll)
    //    {
    //        float maxReputation = float.MinValue;
    //        IEnumerator<IReputable> enumMax = null;
    //        IEnumerator<IReputable> enumIt = coll.GetEnumerator();
    //        while (enumIt.MoveNext())
    //        {
    //            float reputation = float.MinValue;
    //            if (getReputation(enumIt.Current, out reputation))
    //            {
    //                if (maxReputation < reputation)
    //                {
    //                    enumMax = enumIt;
    //                }
    //            }
    //            else if (getReputation(enumIt.Current.state, out reputation))
    //            {
    //                if (maxReputation < reputation)
    //                {
    //                    enumMax = enumIt;
    //                }
    //            }
    //        }

    //        return enumMax;
    //    }

    //    /// <summary>
    //    /// Busca dentro de um container de Agentes qual o agente com maior reputação
    //    /// </summary>
    //    /// <param name="objects">Um vetor de agentes</param>
    //    /// <param name="states">Um vetor de States. Caso nenhum agente exista dentro da base de dados, ele usa o estado deles para determinar o com maior reputação</param>
    //    /// <returns>Um enumarator apontando pro item selecionado</returns>
    //    public IEnumerator<IPerception> getTheHighReputation(ICollection<IPerception> coll)
    //    {
    //        float maxReputation = float.MinValue;
    //        IEnumerator<IPerception> enumMax = null;
    //        IEnumerator<IPerception> enumIt = coll.GetEnumerator();
    //        while (enumIt.MoveNext())
    //        {
    //            float reputation = float.MinValue;
    //            IReputable reputable = enumIt.Current as IReputable;
    //            if (!ReferenceEquals(reputable, null))
    //            {
    //                if (getReputation(reputable, out reputation))
    //                {
    //                    if (maxReputation < reputation)
    //                    {
    //                        enumMax = enumIt;
    //                    }
    //                }
    //                else if (getReputation(reputable.state, out reputation))
    //                {
    //                    if (maxReputation < reputation)
    //                    {
    //                        enumMax = enumIt;
    //                    }
    //                }
    //            }
    //        }

    //        return enumMax;
    //    }
    //    //public int getTheHighReputation(IReputable[] reputables)
    //    //{

    //    //    float maxReputation = float.MinValue;
    //    //    int selected = -1;

    //    //    //busca pelo Agente que esteja em mDicAgentReputation e tenha a menor reputação
    //    //    for(int i = 0; i < objects.Length; i++)
    //    //    {
    //    //        float reputation;
    //    //        if (getReputation(objects[i], out reputation))
    //    //        {
    //    //            if (maxReputation < reputation)
    //    //            {
    //    //                selected = i;
    //    //                maxReputation = reputation;
    //    //            }
    //    //        }
    //    //    }

    //    //    //caso nenhum agente do vetor esteja registrado, verificar pelos estadoos dos agentes qual o com maior reputação
    //    //    if (selected < 0)
    //    //    {
    //    //        maxReputation = float.MinValue;
    //    //        for(int i = 0; i < states.Length; i++)
    //    //        {
    //    //            string str = states[i];
    //    //            float reputation = 0f;
    //    //            if (str != "" && getReputation(str, out reputation))
    //    //            {
    //    //                if (maxReputation < reputation)
    //    //                {
    //    //                    selected = i;
    //    //                    maxReputation = reputation;
    //    //                }
    //    //            }
    //    //        }
    //    //    }

    //    //    return selected;
            
    //    //}

    //    #endregion

    //    #region SoftComponent Members

    //    public void draw(Microsoft.Xna.Framework.GameTime time, ref Microsoft.Xna.Framework.Rectangle rectWindow)
    //    {
    //        //faz nada
    //    }

    //    public void update(Microsoft.Xna.Framework.GameTime time)
    //    {
    //        //faz nada.
    //    }

    //    public bool init(edde.GameObject parent)
    //    {
    //        return true;
    //    }

    //    public void ejected()
    //    {
    //    }

    //    public bool ejectMe()
    //    {
    //        return false;
    //    }

    //    public bool enable
    //    {
    //        get { return false; }//falso para que ele não entre no loop de execução
    //    }

    //    #endregion

    //    public bool m_bCrescenteOrder = true;
        
    //    public void setCrescenteSortOrder()
    //    {
    //        m_bCrescenteOrder = true;
    //    }

    //    public void setDecrescenteSortOrder()
    //    {
    //        m_bCrescenteOrder = false ;
    //    }

    //    private int minRelativeValue()
    //    {
    //        return m_bCrescenteOrder ? -1 : +1;
    //    }

    //    private int maxRelativeValue()
    //    {
    //        return m_bCrescenteOrder ? -1 : +1;
    //    }

    //    #region IComparer<IPerception> Members
    //    public int Compare(IReputable x, IReputable y)
    //    {
    //        if (ReferenceEquals(x, null))
    //        {
    //            if (ReferenceEquals(y, null))
    //                return 0;
    //            else
    //                return minRelativeValue();
    //        }
    //        else
    //        {
    //            if (ReferenceEquals(y, null))
    //                return maxRelativeValue();
    //        }

    //        float reputationA, reputationB;
    //        getReputation(x, out reputationA);
    //        getReputation(y, out reputationB);
    //        if (reputationA < reputationB)
    //            return minRelativeValue();
    //        else if (reputationA > reputationB)
    //            return maxRelativeValue();
    //        else
    //            return 0;

    //    }
    //    #endregion 

    //    #region IComparer<IPerception> Members
    //    public int Compare(IPerception x, IPerception y)
    //    {
    //        IReputable GOa = x.objSource as IReputable;
    //        IReputable GOb = y.objSource as IReputable;
    //        if(ReferenceEquals(GOa, null))
    //        {
    //            if (ReferenceEquals(GOb, null))
    //                return 0;
    //            else
    //                return minRelativeValue();
    //        }else
    //        {
    //            if (ReferenceEquals(GOb, null))
    //                return maxRelativeValue();
    //        }

    //        return Compare(GOa, GOb);
    //    }
    //    #endregion

    //    public int memoryCount() { return mDicAgentReputation.Count; }

    //}
}
