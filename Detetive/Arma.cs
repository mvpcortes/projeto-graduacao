﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde;

namespace Detetive
{
    class Arma: SoftComponent
    {
        private GameObject mParent = null;

        private GameObject parent
        {
            get { return mParent; }
            set { mParent = value; }

        }

        #region SoftComponent Members

        public void draw(Microsoft.Xna.Framework.GameTime time, ref Microsoft.Xna.Framework.Rectangle rectWindow)
        {
            //nothing
        }

        public void update(Microsoft.Xna.Framework.GameTime time)
        {
            if (RPG.me.inputControl.isKeyDown(InputControl.RPG_KEY.ACTION_B))
            {
                Shot shot = new Shot(parent);
               // shot.incPosition((parent.getRectBase().length() + 3) * parent.getDirection());
                RPG.me.sceneManager.insertSafeGameObject(shot);

                SomTiro somTiro = new SomTiro(shot);
                RPG.me.sceneManager.insertSafeGameObject(somTiro);
            }
        }

        public bool init(GameObject _parent)
        {
            mParent = _parent;
            return _parent != null;
        }

        public void ejected()
        {
        }

        public bool ejectMe()
        {
            return false;
        }

        public bool enable
        {
            get { return true; }
        }

        #endregion
    }
}
