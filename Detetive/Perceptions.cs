using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using edde;
using edde.objects;
using Detetive;

namespace edd.motsystem
{
    /// <summary>
    /// Percep��o de batida (bater) de um agente para outro
    /// </summary>
    public class Batida : MotivationalSystem.Perception<GameObject, float>
    {
        public Batida(GameObject origem, float forca, float avaliacao)
            : base(origem, forca, avaliacao)
        {
        }
    }

    /// <summary>
    /// Percep��o de conversa de um agente para outro
    /// </summary>
    public class Conversa: MotivationalSystem.Perception<GameObject, string>
    {
        public Conversa(GameObject origem, float distancia, string texto):base(origem, distancia, texto)
        {
        }
    }

    /// <summary>
    /// Percep��o de brincadeira de um agente para outro
    /// </summary>
    public class Brincadeira : MotivationalSystem.Perception<GameObject, float>
    {
        public Brincadeira(GameObject origem, float forca, float avaliacao)
            : base(origem, forca, avaliacao)
        {
        }
    }

    /// <summary>
    /// Percep��o de um agente intimidando outro
    /// </summary>
    public class Intimidacao : MotivationalSystem.Perception<GameObject, float>
    {
         public Intimidacao(GameObject origem, float forca, float avaliacao)
            : base(origem, forca, avaliacao)
        {
        }
    }

    /// <summary>
    /// Classe que representa um toque de algu�m. Ele tem origem de um GameObject e mensagem � um vetor indicando a dire��o do item tocado para o item que tocou.
    /// </summary>
    public class Tocado : MotivationalSystem.Perception<GameObject, Vector2>
    {
         public Tocado(GameObject origem, float forca, Vector2 direcao)
            : base(origem, forca, direcao)
        {
            data.Normalize();
        }
    }

    /// <summary>
    /// Classe que representa a percep��o de ouvir um tiro
    /// </summary>
    public class OuvirTiro : MotivationalSystem.Perception<Vector2, Vector2>
    {
        public OuvirTiro(Vector2 pos, float intensity, Vector2 direcao  ):
            base(pos, intensity, direcao)
        {
        }
    }

    /// <summary>
    /// PResente dado por um her�i a um NPC
    /// </summary>
    public class Presente : MotivationalSystem.Perception<DetetiveHero, GameObject>
    {
        public Presente(DetetiveHero pos, float intensity, GameObject presente) :
            base(pos, intensity, presente)
        {
        }
    }

    /// <summary>
    /// Classe que representa uma perce��o de vis�o
    /// </summary>
    public class ViuDesconhecido : MotivationalSystem.Perception<GameObject, Vector2>, IComparable<ViuDesconhecido>
    {
        public ViuDesconhecido(GameObject origem, float distancia, Vector2 direcao):
        base(origem, distancia, direcao)
        {
        }

        #region IComparable<ViuDesconhecido> Members

        public int CompareTo(ViuDesconhecido other)
        {
            float f = this.intensity - other.intensity;
            if (f > 0) return 1;
            else if(f < 0) return -1;
            else return 0;
        }

        #endregion
    }
}
