using System;
using System.Collections.Generic;
using System.Text;
using edde.components;
using edde.components.motsystem; 
using edde;
using MotivationalSystem;

namespace edd.motsystem
{
    using MyTimer = edde.components.Timer<MotivationalObject>;
    using edd.motsystem;
    using Microsoft.Xna.Framework;
    using edde;
using Microsoft.Xna.Framework.Graphics;
    using edde.physicalmanager;


    class MotivationalObject: GameObject, IMotivationalObject
    {
        //Determina poss�veis donos do movimento
        public enum MOVER_OWNER
        {
            NONE  = -1,
            FIRST = 0,
            FUGIR = 0,
            PROCURAR,
            PROCURAR_BRINQUEDO,
            RANDOM,
            EXPLORAR,
            COUNT
        };

        /// <summary>
        /// Diz quem � o dono do movimento e qual o valor de intensidade deles
        /// </summary>
        //PriorityMoverOwner mPriorityMoverOwner;
        MOVER_OWNER mMoverOwner = MOVER_OWNER.NONE;
        

        /// <summary>
        /// Tamanho da diagonal do charset.
        /// </summary>
        private static readonly float CHAR_SIDE = 16;//(float)GameObject.DEFAULT_ICOLLISED_REGION.getSize().Length();

        private const int BALLON_CONVERSAR  = 15;
        private const int BALLON_BRINCAR    = 16;
        private const int BALLON_BATER      = 17;
        private const int BALLON_DORMIR     = 08;
        private static readonly TimeSpan TIME_BRINCAR       = TimeSpan.FromSeconds(0.5);
        private static readonly TimeSpan TIME_CONVERSAR     = TimeSpan.FromSeconds(1);
        private static readonly TimeSpan TIME_BATER         = TimeSpan.FromSeconds(0.5);
        private static readonly TimeSpan TIME_DORMIR_RATE   = TimeSpan.FromSeconds(20);

        #region fields Injection
        //[Injection]
        //private GameGrid mGrid;

        [Injection]
        private TextComponent mTextID;

        [Injection]
        private CharSetComponent mCharSet;

        [Injection]
        private MotSystemComponent mMotivational;

        [Injection]
        private Mover mMoverComponent;

        [Injection]
        private MyTimer mTimer;

        [Injection]
        private TileAnimationComponent mTileAnimation;


        #endregion

        #region ISensor fields
        private ISensor mSensorViuDesconhecido;

        private ISensor mSensorViuBrinquedo;

        private ISensor mSensorBrincado;

        private ISensor mSensorBatida;

        private ISensor mSensorConversado;
        #endregion ISensor fields

        #region others fields

        private string mName;


        //!� preciso detectar o tempo de update para evitar o teste de vis�o e a atualiza��o dos sensores.
        private bool mIsTimeUpdate;

        /// <summary>
        /// Verifica qual foi o ultimo lado para onde este agente girou. true  � direito e false � esquerdo
        /// </summary>
        private bool mLastSideUse;

        /// <summary>
        /// Qual o �ngulo o �ngulo acumulativo girado para a �ltima dire��o
        /// </summary>
        /// <remarks>
        /// O sistema sempre que precisa olhar para algum lugar, gira para um lado em 360�. Quando n�o acha, ele tenta girar para o outro lado.
        /// </remarks>
        private float mAcumAngle;

        /// <summary>
        /// Refer�ncia ao objeto que o agente tocou no �ltimo update.
        /// </summary>
        /// <remarks>
        /// � preciso armazenar a ref�r�ncia a um elemento que sofreu toque no atual momento.
        /// </remarks>
        private IMotivationalObject mOtherTouch;

        /// <summary>
        /// Refer�ncia a Toy encostado no �ltimo update;
        /// </summary>
        private Toy mToyTouch;

        /// <summary>
        /// verifica se est� executando a conversa
        /// </summary>
        private bool mExecutingConversar = false;

        /// <summary>
        /// Se o comportamento de brincar est� sendo executado
        /// </summary>
        private bool mExecutingBrincar = false;

        /// <summary>
        /// Se o comportamento bater est� sendo executado
        /// </summary>
        private bool mExecutingBater = false;

        /// <summary>
        /// Verifica se o comportamento dormir est� sendo executado
        /// </summary>
        private bool mExecutingDormir = false;

        /// <summary>
        /// Verifica se algum comportamento mais priorit�rios est�o ocorrendo.
        /// </summary>
        /// <returns></returns>
        private bool isExecutingAnyUpAction() { return mExecutingBrincar || mExecutingConversar || mExecutingBater || mExecutingDormir; }

        private bool isExecutingAction() { return isExecutingAnyUpAction() || mMoverOwner != MOVER_OWNER.NONE; }
        #endregion

        #region properties
        public string name
        {
            get { return mName; }
            set { mName = value; }
        }

        private float acumAngle
        {
            get { return mAcumAngle; }
            //set { mAcumAngle = value % Math.PI * 2; }
        }

        private bool lastSide
        {
            get { return mLastSideUse; }
        }

        private Color colorShandow
        {
            get { return mCharSet.colorShandow; }
            set { mCharSet.colorShandow = value; }
        }

        private void incAcumAngle (float inc)
        {
            float temp = acumAngle + inc;
            if (temp > Math.PI * 2)
            {
                mLastSideUse = !mLastSideUse;
                temp = temp % ((float)Math.PI * 2f);
            }

            mAcumAngle = temp;
        }

        private void resetAcumAngle()
        {
            mAcumAngle = 0f;
        }

        internal IMotivationalObject otherTouch
        {
            get { return mOtherTouch; }
        }

        internal Toy toyTouch
        {
            get { return mToyTouch; }
        }

        internal bool hasOtherTouch()
        {
            return !ReferenceEquals(mOtherTouch, null); 
        }

        internal bool hasToyTouch()
        {
            return !ReferenceEquals(mToyTouch, null);
        }

        internal string textID
        {
            get { return mTextID.text ; }
            set { mTextID.text = value; }
        }
        #endregion

        /// <summary>
        /// Construtor
        /// </summary>
        public MotivationalObject()
        {
            mCharSet = new CharSetComponent("amanda");
            mMotivational = new MotSystemComponent("npc");
            mTimer = new Timer<MotivationalObject>();
            mMoverComponent = new Mover();
            mTileAnimation = new TileAnimationComponent("ballon", TileSet.INVALID_TILESET);
            mTileAnimation.pointCenter = new Point(mTileAnimation.pointCenter.X -10, mTileAnimation.pointCenter.Y);

            mTextID = new TextComponent("arial_8");
            mTextID.scale = 2;
            mTextID.posCoor = new Vector2(+10, 0);
            //mTimer.addTimerLoop(TimeSpan.FromSeconds(1), TimeSpan.Zero, OnTimeMotivationalProcess);
            mName = "sem nome";
            mToyTouch = null;

            mIsTimeUpdate = false;
        }

        public bool isMotUpdateTime()
        {
            return mIsTimeUpdate;
        }

        #region IMotivationalObject members

        public void sendBatida(Batida batida)
        {
            mSensorBatida.sendPerception(batida);
        }

        /// <summary>
        /// Envia para este agente uma percep��o de conversa. Normalmente outro agente realiza isto.
        /// </summary>
        /// <param name="conversa">Perception sobre uma conversa</param>
        public void sendConversa(Conversa conversa)
        {
            mSensorConversado.sendPerception(conversa);
        }

 
        /// <summary>
        /// Envia para este agente uma percep��o de brincadeira. Normalmente outro agente realiza isto.
        /// </summary>
        /// <param name="brinca">Um Perception representando a brincadeira</param>
        public void sendBrincadeira(Brincadeira brinca)
        {
           mSensorBrincado.sendPerception(brinca);
        }


        /// <summary>
        /// Envia para este agente uma percep��o sobre um outro agente desconhecido dentro do campo de vis�o. Normalmente � enviado por si mesmo.
        /// </summary>
        /// <param name="viu">Um perception representando o agente desconhedcido</param>
        private void sendViuDesconhecido(ViuDesconhecido viu)
        {
            mSensorViuDesconhecido.sendPerception(viu);
        }

        private void sendViuBrinquedo(ViuBrinquedo viu)
        {
            mSensorViuBrinquedo.sendPerception(viu);
        }

        #endregion

        #region Time Events
        private void OnTimeStopConversar(MyTimer.TimeParam timeParam, MotivationalObject parent)
        {
            mTileAnimation.actualTile = TileSet.INVALID_TILESET;
            mExecutingConversar = false;
        }

        private void OnTimeStopBrincar(MyTimer.TimeParam timeParam, MotivationalObject parent)
        {
            mTileAnimation.actualTile = TileSet.INVALID_TILESET;
            mExecutingBrincar = false;
        }


        private void OnTimeStopBater(MyTimer.TimeParam timeParam, MotivationalObject parent)
        {
            mTileAnimation.actualTile = TileSet.INVALID_TILESET;
            mExecutingBater = false;
        }

        private void OnTimeStopDormir(MyTimer.TimeParam timeParam, MotivationalObject parent)
        {
            mTileAnimation.actualTile = TileSet.INVALID_TILESET;
            mExecutingBater = false;
        }
        #endregion

        public ICollection<IProcessor> ContentProcessor
        {
            get { return mMotivational.ContentProcessor; }
        }

        public void EndMoviment()
        {
          mMoverOwner = MOVER_OWNER.NONE;
        }

        #region GameObject members
        /// <summary>
        /// Inicia o Agente
        /// </summary>
        /// <param name="scene">A Scene a qual ele pertence.</param>
        /// <returns></returns>
        public override sealed bool init(Scene scene)
        {
            if (!base.init(scene)) return false;

            //mGrid.enable = false;
  
            //carrega sensores
            try
            {
                mSensorViuDesconhecido      = mMotivational.getSensor("VerDesconhecido");
                //mSensorViuBrinquedo         = mMotivational.getSensor("VerBrinquedo");
                mSensorBrincado             = mMotivational.getSensor("Brincado");
                mSensorConversado           = mMotivational.getSensor("Conversado");
                mSensorBatida               = mMotivational.getSensor("Batido");

                mMotivational.setAction("Brincar"       , new ActionDelegate(runBrincar, isRunBrincar));
                mMotivational.setAction("Conversar"     , new ActionDelegate(runConversar, isRunConversar));
                mMotivational.setAction("Bater"         , new ActionDelegate(runBater, isRunBater));
                mMotivational.setAction("Fugir"         , new ActionDelegate(runFugir, isRunFugir));
                mMotivational.setAction("AndarAleatoriamente", new ActionDelegate(runRandomMoviment, isRunRandomMoviment));
                mMotivational.setAction("Procurar"      , new ActionDelegate(runProcurar, isRunProcurar));
                mMotivational.setAction("Explorar"      , new ActionDelegate(runExplorar, isRunExplorar));

            }
            catch (MotivationalSystemException p)
            {
                return false;
            }

            /////Atualiza os valores da grid pela primeira vez.
            //{
            //    int rowCount = mMotivational.ContentProcessor.Count + 1;

            //    mGrid.setSize(rowCount, 3);

            //    mGrid.setColWidth(0, 100);
            //    mGrid.setColWidth(1, 60);
            //    mGrid.setColWidth(2, 60);

            //    int i = 1;

            //    foreach (Processor p in mMotivational.ContentProcessor)
            //    {
            //        mGrid[i, 0].mText= p.name;
            //        i++;
            //    }
            //}

            //atualiza o mover
            mMoverComponent.OnEndAllMoviment += EndMoviment;
            return true;
        }

        public override sealed void update(Microsoft.Xna.Framework.GameTime time)
        {
            base.update(time);
            //mOtherTouch = null;
            float[] f = mMotivational.getEmotionalColor();
            colorShandow = new Color(f[0], f[1], f[2], 1);
        }

        public sealed override void draw(GameTime time, ref Rectangle rectWindow)
        {
            base.draw(time, ref rectWindow);
        }

        public override void onTouch(ICollection<edde.physicalmanager.ICollised> collObject)
        {
            mOtherTouch = null;
            mToyTouch   = null;

            foreach (edde.physicalmanager.ICollised collised in collObject)
            {
                mOtherTouch = collised as IMotivationalObject;
                mToyTouch   = collised as Toy;
                if (!ReferenceEquals(mOtherTouch, null) && !ReferenceEquals(mToyTouch, null)) break;
            }
        }
    
        public override sealed bool testVision(GameTime gameTime)
        {
            mIsTimeUpdate = mMotivational.isUpdateTime(gameTime.TotalGameTime);
            return isMotUpdateTime();
        }


        public override sealed void verifyVisionContainer(ICollection<edde.physicalmanager.ICollised> collVision)
        {
            foreach (edde.physicalmanager.ICollised collised in collVision)
            {
                GameObject go = collised as MotivationalObject;
                if(go == null)
                    go = collised as edde.objects.Hero;

                if (!ReferenceEquals(go, null))
                {
                   Vector2 visionDir = new Vector2();
                   Vector2 vecDist = visionDir = (go.position- this.position);
                   visionDir.Normalize();
                   float intensify = (float)(Collision.VISION_DISTANCE - (float)vecDist.Length()) / (float)Collision.VISION_DISTANCE;
                   if(intensify >= 0f)
                   {
                        ViuDesconhecido p = new ViuDesconhecido((GameObject)go, intensify,  visionDir);
                        sendViuDesconhecido(p);
                   }
                }
            }
        }

        #endregion GameObject members


        #region IsRun delegates

        List<IPerception> ms_listRelevancePerceptions = new List<IPerception>();

        private bool isRunBater(out float efficiency)
        {
            if (mExecutingBater)
            {
                efficiency = 1;
                return true;
            }
            else
            {
                efficiency = 0;
                return false;
            }
        }

        private bool isRunDormir(out float efficiency)
        {
            if (mExecutingDormir)
            {
                efficiency = 1;
                return true;
            }
            else
            {
                efficiency = 0;
                return false;
            }
        }

        private bool isRunBrincar(out float efficiency)
        {
            if (mExecutingBrincar)
            {
                efficiency = 1;
                return true;
            }
            else
            {
                efficiency = 0;
                return false;
            }
        }
       
        private bool isRunConversar(out float efficiency)
        {
            if (mExecutingConversar)
            {
                efficiency = 1;
                return true;
            }
            else
            {
                efficiency = 0;
                return false;
            }
        } 
   
        private bool isRunFugir(out float efficiency)
        {
            if (mMoverOwner == MOVER_OWNER.FUGIR)
            {
                efficiency = mMoverComponent.accomplishActualMoviment();
                return true;
            }
            else
            {
                efficiency = 0;
                return false;
            }
        }

        private bool isRunRandomMoviment(out float efficiency)
        {
            if (mMoverOwner == MOVER_OWNER.RANDOM)
            {
                efficiency = 1;
                return true;
            }
            else
            {
                efficiency = 0;
                return false;
            }
        }

        private bool isRunExplorar(out float efficiency)
        {
            if (mMoverOwner == MOVER_OWNER.EXPLORAR)
            {
                efficiency = 1;
                return true;
            }
            else
            {
                efficiency = 0;
                return false;
            }
        }

        private bool isRunProcurar(out float efficiency)
        {
            if (mMoverOwner == MOVER_OWNER.PROCURAR)
            {
                efficiency = mMoverComponent.accomplishActualMoviment();
                return true;
            }
            else
            {
                efficiency = 0;
                return false;
            }
        }

        private bool isRunProcurarBrinquedo(out float efficiency)
        {
            if (mMoverOwner == MOVER_OWNER.PROCURAR_BRINQUEDO)
            {
                efficiency = mMoverComponent.accomplishActualMoviment();
                return true;
            }
            else
            {
                efficiency = 0;
                return false;
            }
        }
        #endregion IsRun delegates

        #region Run delegates

        private bool runDormir(float intensity)
        {
            if (isExecutingAnyUpAction()) return false;

            mMoverComponent.stopAll();

            mExecutingDormir = true;
            mTimer.addTimer(TimeSpan.FromSeconds((float)TIME_DORMIR_RATE.TotalSeconds*intensity), OnTimeStopBater);
            mTileAnimation.actualTile = BALLON_DORMIR;
            return true;
        }
        private bool runBater(float intensity)
        {
            if (isExecutingAnyUpAction()) return false;

            if (!hasOtherTouch()) return false;

            mMoverComponent.stopAll();

            float value = (position - otherTouch.getRectBase().getCenter()).Length();
            value = Math.Min(value / CHAR_SIDE, 1);
            otherTouch.sendBatida(new Batida(this, value, 1));
            mExecutingBater = true;
            mTimer.addTimer(TIME_BATER, OnTimeStopBater);
            mTileAnimation.actualTile = BALLON_BATER;
            return true;
        }

        private bool runBrincar(float intensity)
        {
            if (isExecutingAnyUpAction()) return false;

            if (hasOtherTouch())
            {
                mMoverComponent.stopAll();

                float value = (position - otherTouch.getRectBase().getCenter()).Length();
                value = Math.Min(value / CHAR_SIDE, 1);
                otherTouch.sendBrincadeira(new Brincadeira(this, value, 1));
                mExecutingBrincar = true;
                mTimer.addTimer(TIME_BRINCAR, OnTimeStopBrincar);
                mTileAnimation.actualTile = BALLON_BRINCAR;
                return true;
            }

            if (hasToyTouch())
            {
                mMoverComponent.stopAll();
                float value = (position - toyTouch.getRectBase().getCenter()).Length();
                value = Math.Min(value / CHAR_SIDE, 1);
                mExecutingConversar = true;
                mTimer.addTimer(TimeSpan.FromSeconds(0.5), OnTimeStopBrincar);
                mTileAnimation.actualTile = BALLON_BRINCAR;
                return true;
            }
            return false;
        }
    
        private bool runConversar(float intensity)
        {
            if (isExecutingAnyUpAction()) return false;

            if(!hasOtherTouch()) return false;

            mMoverComponent.stopAll();

            float value = (position - otherTouch.getRectBase().getCenter()).Length();
            value = Math.Min( value / CHAR_SIDE, 1);
            otherTouch.sendConversa(new Conversa(this, value, "ola"));
            mExecutingConversar = true;
            mTimer.addTimer(TIME_CONVERSAR, OnTimeStopConversar);
            mTileAnimation.actualTile = BALLON_CONVERSAR;
            return true;
           
        }

        /// <summary>
        /// Gera um movimento para frente do personagem de acordo com uma itensidade
        /// </summary>
        /// <param name="intensity">Quando (velocidade) o npc ir� andar (n�o sendo usado)</param>
        private void genericIntensityMove(float intensity)
        {
            mMoverComponent.addMove(TimeSpan.FromSeconds(0.5), 15);
        }
         private bool genericRunProcurar(List<IPerception> list, float intensity)
        {
            
            bool girou = false;
            //se n�o tem nada, continua no mesmo caminho
            if (list.Count != 0)
            {
                if (list.Count > 1)
                {
                    int a = 0;
                }
                list.Sort();
                GameObject obj = list[MyMath.getRandom(list.Count)].objSource as GameObject;
                if (!ReferenceEquals(obj, null))
                {
                    Vector2 dif = obj.position - position;
                    if (dif.Length() < CHAR_SIDE)
                    {
                        mMoverComponent.stopAll();
                        return false; //j� encontrou algu�m pr�ximo o suficiente.
                    }
                    setDirection(dif);
                    girou = true;
                }
            }

            //for�a girar para procurar algu�m
            if (!girou)
            {
                incAcumAngle((float)Math.PI / 4f);
                Vector2 temp = this.getDirection();
                temp = MyMath.rotate(temp, (float)((lastSide ? +1 : -1) * Math.PI / 4f));
                setDirection(temp);
            }
            else
                resetAcumAngle();

            genericIntensityMove(intensity);
            return true;
        }


        private bool runProcurarBrinquedo(float intensity)
        {
            if (isExecutingAction())
                return false;


            mSensorViuBrinquedo.getRelevancePerceptions(ms_listRelevancePerceptions);
            if (ms_listRelevancePerceptions.Count > 0)
            {
                if (genericRunProcurar(ms_listRelevancePerceptions, intensity))
                    mMoverOwner = MOVER_OWNER.PROCURAR_BRINQUEDO;
                return true;
            }
            return false;
        }

        private bool runProcurar(float intensity)
        {
            if (isExecutingAction())
                return false;

            mSensorViuDesconhecido.getRelevancePerceptions(ms_listRelevancePerceptions);
            if (ms_listRelevancePerceptions.Count > 0)
            {
                if(genericRunProcurar(ms_listRelevancePerceptions, intensity))
                    mMoverOwner = MOVER_OWNER.PROCURAR;

                return true;
            }
            return false;
        }

        private bool runFugir(float intensity)
        {
            if (isExecutingAction()) 
                return false;

            mSensorViuDesconhecido.getRelevancePerceptions(ms_listRelevancePerceptions);

            if (ms_listRelevancePerceptions.Count != 0)
            {
                //a fulga � uma combina��o linear inversa das percep��es obtidas
                Vector2 vec = Vector2.Zero;
                foreach (ViuDesconhecido viu in ms_listRelevancePerceptions)
                {
                    vec += position - viu.source.position;
                }

                vec.Normalize();
                this.setDirection(vec);
            }

            genericIntensityMove(intensity);
            mMoverOwner = MOVER_OWNER.FUGIR;
            return true;
        }
        private bool runRandomMoviment(float intensity)
        {
            if (isExecutingAction()) return false;

            setDirection(edde.MyMath.getRandomDir());
            genericIntensityMove(intensity);
            mMoverOwner = MOVER_OWNER.RANDOM;
            return true;
        }

        private bool runExplorar(float intensity)
        {
            if (isExecutingAction()) return false;

            setDirection(edde.MyMath.getRandomDir());
            //mMoverComponent.addMove(TimeSpan.FromSeconds(1), 10);
            genericIntensityMove(intensity);
            mMoverOwner = MOVER_OWNER.EXPLORAR;
            return true;
        }
  
        #endregion Run delegates
    }
}