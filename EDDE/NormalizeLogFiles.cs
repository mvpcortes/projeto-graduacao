﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using util;

namespace edde
{
    /// <summary>
    /// Sistema de log da engine. Está muito específico para problema do projeto final
    /// </summary>
    public class NormalizeLogFiles
    {
        StreamReader mSourceFile;

        StreamWriter mDestinyFile;

        List<float> mListMediaData;

        LinkedList<List<float>> mMatrixLineData;

        LinkedList<string> mListEmotion;

        public string mStrHead;
        public NormalizeLogFiles(string namefile)
        {
            mSourceFile = new StreamReader("C:\\Detetive\\"+namefile+".log");

            namefile += "_normalized";
            mDestinyFile = new StreamWriter("C:\\Detetive\\" + namefile + ".log");

            mStrHead = mSourceFile.ReadLine();

            string[] tokenize = mStrHead.Split('\t');
            mListMediaData = new List<float>(tokenize.Length-1);

            for (int i = 0; i < tokenize.Length-1; i++)
                mListMediaData.Add(0f);

            mMatrixLineData = new LinkedList<List<float>>();
            mListEmotion = new LinkedList<string>();
            readData();
        }

        private void readData()
        {
            while (!mSourceFile.EndOfStream)
            {
                string strTemp = mSourceFile.ReadLine();
                int newSize = strTemp.Length - 1;
                strTemp = strTemp.Substring(0,  newSize);
                int posTable = strTemp.LastIndexOf('\t');

                string emotion = strTemp.Substring(posTable);
                emotion = emotion.Substring(1, emotion.Length-1);
                strTemp = strTemp.Substring(0, posTable+1);

                mListEmotion.AddLast(emotion);
                float [] values = StrTools.toVecFloat(strTemp, '\t');

                List<float> tempList = new List<float>(mListMediaData.Count);
               
                for (int i = 0; i < values.Length; i++)
                {
                    float value = values[i];
                    mListMediaData[i] = Math.Max(mListMediaData[i], Math.Abs(value));
                    tempList.Add(value);
                }
                
                mMatrixLineData.AddLast(tempList);
            }

            //verifica zero da lista de média
            for (int i = 0; i < mListMediaData.Count; i++)
            {
                if (Math.Abs(mListMediaData[i]) < 0.000001)
                    mListMediaData[i] = 1f;
            }


            //grava dados
            mDestinyFile.WriteLine(mStrHead);
            LinkedListNode<string> nodeEmotion = mListEmotion.First;
            foreach (List<float> tempList in mMatrixLineData)
            {
                for(int i=0; i< tempList.Count; i++ )
                {
                    mDestinyFile.Write(tempList[i] / mListMediaData[i]);
                    mDestinyFile.Write("\t");
                }
                mDestinyFile.WriteLine(nodeEmotion.Value + "\t");

                nodeEmotion = nodeEmotion.Next;
                if (ReferenceEquals(nodeEmotion, null)) break;
            }

            mSourceFile.Close();
            mDestinyFile.Close();
        }
    }
}
