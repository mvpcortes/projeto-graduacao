using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace edde.physicalmanager
{
    /*
    class QuadTreeNode: ICollisionManager
    {
        public enum CHILD_NODE
        { 
            VOID    = -1,       //#####
            FIRST   =  0,       //#1#2#
            SECOND  =  1,       //#####
            THIRD   =  2,       //#4#3#
            FOUR    =  3,       //#####
            LENGTH  =  4
        }
        private RectFloat mRect; //rect que representa a �rea da c�lula

        private QuadTreeNode[] mQuadTreeChilds = null;

        private LinkedList<ICollised> mListCollised;

        private const int MAX_POPULATION        = 25;
        private const int MAX_NIVEL             = 10;

        private int mnNivel                     = 0;

        private int mnPopulation                = 0;

        public void setNivel(int n) { mnNivel = n; }

        public int getNivel() { return mnNivel; }

        public RectFloat getRect()
        {
            return mRect;
        }

        public void setRect(RectFloat rect)
        {
            mRect = rect;
        }

        public void transformRect(CHILD_NODE node)
        {
            RectFloat rect = getRect();
            switch(node)
            {
                case CHILD_NODE.FIRST:
                break;

                case CHILD_NODE.SECOND:
                    mRect.setSource(rect.left + rect.width/2, rect.top);
                break;

                case CHILD_NODE.THIRD:
                    mRect.setSource(rect.left + rect.width/2, rect.top + rect.height/2);
                break;

                case CHILD_NODE.FOUR:
                    mRect.setSource(rect.left, rect.top + rect.height/2);
                break;
            }

            mRect.setSize(rect.width/2, rect.height/2);
        }



        public QuadTreeNode(float w, float h): this(new RectFloat(0f,0f, w, h))
        {
        }

        public QuadTreeNode(RectFloat rect)
        {
            mQuadTreeChilds = null;
            mListCollised   = new LinkedList<ICollised>();
            mRect = rect;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        public QuadTreeNode getChild(CHILD_NODE child)
        {
            if(child == CHILD_NODE.VOID) return null;
            if(isChild()) return null;

            return mQuadTreeChilds[(int)child];
        }

        public ICollection<ICollised> getColliseds()
        {
            if(!isChild()) return null;

            return mListCollised;
        }

        public int getPopulation()
        {
            if(isChild())
            {
                return mListCollised.Count;
            }else
            {
                
                int qtd = 0;
                foreach(QuadTreeNode qt in mQuadTreeChilds)
                {
                    qtd+= qt.getPopulation();
                }
                return qtd;
                
                //return mnPopulation;
            }
        }

        public bool isChild()
        {
            return ReferenceEquals(mQuadTreeChilds, null);
        }

        public void verifyPopulation()
        {
            if (isChild())
            {
                if ((getPopulation() > MAX_POPULATION))
                {
                    expandNode();
                }
            }
            else
            {
                if ((getPopulation() <= MAX_POPULATION))
                    compactNode();
            }
        }

        private void expandNode()
        {
            if(!isChild()) return;
            if (getNivel() > MAX_NIVEL) return;

            mQuadTreeChilds = new QuadTreeNode[(int)CHILD_NODE.LENGTH];
            for(int i = 0; i < (int)CHILD_NODE.LENGTH; i++)
            {
                QuadTreeNode qt = mQuadTreeChilds[i] = new QuadTreeNode(mRect);
                qt.transformRect((CHILD_NODE)i);
                qt.setNivel(getNivel() + 1);
            }

            while(mListCollised.Count > 0)
            {
                ICollised collised = mListCollised.First.Value;
                mListCollised.RemoveFirst();
                foreach(QuadTreeNode qt in mQuadTreeChilds)
                {
                    if(Collision.intersects(collised.getRectBase(), qt.getRect()))
                        qt.addObject(collised);
                }
            }
        }

        private void compactNode()
        {
            if(isChild()) return;

            mListCollised.Clear();
            foreach(QuadTreeNode qt in mQuadTreeChilds)
            {
                qt.compactNode();
                foreach (ICollised collised in qt.getColliseds())
                {
                    if(!mListCollised.Contains(collised))
                        mListCollised.AddLast(collised);
                }
            }
            mnPopulation = mListCollised.Count;
            mQuadTreeChilds = null;
        }
    
    #region ICollisionManager Members

    public void  setSizeRegion(int w, int h)
    {
        mRect.setSize(new Vector2(w, h));
        compactNode();
        verifyPopulation();
    }

    public void  addObject(ICollised go)
    {
        if(Collision.intersects(go.getRectBase(), getRect()))
        {
            mnPopulation++;
            if(isChild())
            {
                mListCollised.AddLast(go);
                verifyPopulation();
            }else
            {
                foreach(QuadTreeNode qt in mQuadTreeChilds)
                {
                    qt.addObject(go);
                }
            }
        }
    }

    public void  removeObject(ICollised go)
    {
        if(Collision.intersects(go.getRectBase(), getRect()))
        {
            mnPopulation--;
            if(isChild())
            {
                mListCollised.Remove(go);
            }else
            {
                int childs = 0;
                foreach(QuadTreeNode qt in mQuadTreeChilds)
                {
                    qt.removeObject(go);
                    if (qt.isChild()) childs++;
                }

                if(childs  == (int)CHILD_NODE.LENGTH)
                    verifyPopulation();
            }
        }
    }

    public void  testColision(GameObject obj, ICollection<ICollised> collCollision, ICollection<ICollised> collTouch)
    {
       if (!Collision.isCollisionType(obj.getTypeCollision())) return;

        RectFloat rectObj = obj.getRectBase();

        if(Collision.intersects(rectObj, getRect()))
        {
            if(isChild())
            {
                foreach(ICollised other in mListCollised)
                {
                    if (!ReferenceEquals(obj, other))
                    {
                        RectFloat otherRect = other.getRectBase();
                        if (Collision.intersects(otherRect, rectObj) && Collision.isCollisionType(other.getTypeCollision()))
                        {
                            collCollision.Add(other);
                        }
                        else
                        {
                            otherRect.expandFromScale(Collision.TOUCH_LIMIT_ESCALE);
                            if (Collision.intersects(otherRect, rectObj) && Collision.isTouchType(other.getTypeCollision()))
                                collTouch.Add(other);
                        }
                    }
                }
            }
            else
            {
                foreach(QuadTreeNode qt in mQuadTreeChilds)
                {
                    qt.testColision(obj, collCollision, collTouch);
                }
            }
        }
    }

        public void testVision(TriangleCamera camera, ICollection<ICollised> collVisible)
        { }
        public void testVision(RectFloat rect, ICollection<ICollised> collVisible)
        { }

    #endregion

    #region ICollisionManager Members


        private void DebugMe()
        {
            int i = getNivel();
            while (i > 0)
            {
                Debug.Write("\t");
                i--;
            }
            Debug.WriteLine(getPopulation());
            if (!isChild())
            {
                foreach (QuadTreeNode qt in mQuadTreeChilds)
                {
                    qt.DebugMe();
                }
            }
        }
    #endregion
}*/
}
