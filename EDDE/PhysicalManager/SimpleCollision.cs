using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace edde.physicalmanager
{
    /// <summary>
    /// Implementa um sistema de colis�o que verifica a colis�o de um elemento com todos os outros, sem nenhuma otimiza��o. Gerando um algoritmo n*n
    /// </summary>
    internal class SimpleCollision: IPhysicalManager
    {
        List<ICollised> mListCollised =  new List<ICollised>();

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="map">Este sistema depende do Map pos pre</param>
        public SimpleCollision(ICollection<ICollised> collICollised)
        {
            foreach (ICollised collised in collICollised)
            {
                mListCollised.Add(collised);
            }
        }

        #region ICollisionManager Members

        /// <summary>
        /// Este m�todo n�o faz nada pois esta implementa��o n�o precisa do tamanho do mapa.
        /// </summary>
        /// <param name="w"></param>
        /// <param name="h"></param>
        public void setSizeRegion(int w, int h)
        {
            //n�o usado
        }

        /// <summary>
        /// Este m�todo n�o faz nada pois o sistema possui uma refer�ncia ao container de GameObject do Map.
        /// </summary>
        /// <param name="go"></param>
        public void addObject(ICollised go)
        {
            mListCollised.Add(go);
        }

        /// <summary>
        /// Este m�todo n�o faz nada pois o sistema possui uma refer�ncia ao container de GameObject do Map.
        /// </summary>
        /// <param name="go"></param>
        public void removeObject(ICollised go)
        {
            mListCollised.Remove(go);
        }

     
        public void testColision(GameObject obj, ICollection<ICollised> collCollision, ICollection<ICollised> collTouch)
        {
            if (!Collision.isCollisionType(obj.getTypeCollision())) return;

            RectFloat rectObj = obj.getRectBase();
            RectFloat rectTouch = rectObj;
            rectTouch.expandFromScale(Collision.TOUCH_LIMIT_ESCALE);

            foreach (ICollised go in mListCollised)
            {
                if (!ReferenceEquals(go, obj))
                {
                    if (go.intersects(rectObj) && Collision.isCollisionType(go.getTypeCollision()))
                        collCollision.Add(go);
                    else if (go.intersects(rectObj) && Collision.isTouchType(go.getTypeCollision()))
                        collTouch.Add(go);
                }
            }  
        }

        public void testVision(GameObject obj,ICollection<ICollised> collVisible)
        {
            Vector2 vecVision = obj.getDirection();
            vecVision.Normalize();

            foreach (ICollised coll in mListCollised)
            {
                if (!ReferenceEquals(coll, obj))
                {
                    Vector2 vecDist = coll.getRectBase().getCenter() + obj.position;
                    if (vecDist.Length() < Collision.VISION_DISTANCE)
                        collVisible.Add(coll);

                    vecDist.Normalize();
                    float angle = (float)Math.Acos((double)Vector2.Dot(vecDist, vecVision));
                    if(angle < Collision.VISION_ANGLE)
                        collVisible.Add(coll);
                }

             
            }
        }

        public bool findPathTo(GameObject source, Vector2 destiny, ICollection<Vector2> path)
        {
            return false;
        }

        void IPhysicalManager.getObjectRegion(Vector2 centerPoint, float range, ICollection<ICollised> collObjRegion)
        {
            collObjRegion.Clear();

            foreach (ICollised other in mListCollised)
            {
                if((other.getRectBase().getCenter() - centerPoint).Length() < range)
                {
                    collObjRegion.Add(other);
                }
            }
        }

        #endregion
    }
}
