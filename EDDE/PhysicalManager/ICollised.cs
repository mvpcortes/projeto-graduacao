using System;
using System.Collections.Generic;
using System.Text;

namespace edde.physicalmanager
{
    /// <summary>
    /// interface para objetos de colis�o. Como GameObjects e Walls
    /// </summary>
    public interface ICollised
    {
       RectFloat getRectBase();

       COLLISION_TYPE getTypeCollision();

       bool intersects(RectFloat rect);
    }
}
