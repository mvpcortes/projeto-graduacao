using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using System.Xml;

namespace edde
{
    /// <summary>
    /// Classe que abstrai os detalhes de leitura de recursos e o  padr�o de armazenamento de arquivos
    /// <remarks>
    /// Isto � um singletons
    /// </remarks>
    /// </summary>
    public static class Resource
    {
        public static readonly Color DEFAULT_TRANSPARENT_COLOR = new Color(0, 255, 0, 255);

        public const string PATH_TILESET = "tileset\\";
        public const string PATH_CHARSET = "charset\\";
        public const string PATH_FONT    = "font\\";
        public const string PATH_FRAME   = "frame\\";
        public const string PATH_MAP     = "\\map\\";
        public const string PATH_XML     = "\\xml\\";

        public const string EXT_XML = ".xml";
        public const string EXT_MAP = ".tmx";

        private static Dictionary<string, KeyValuePair<Texture2D, int>> mDicLoadedResource = new Dictionary<string, KeyValuePair<Texture2D, int>>();

        #region TextureManager


        /// <summary>
        /// Verifica se a textura j� n�o foi carregada e a retorna caso positivo
        /// </summary>
        /// <remarks>Isto funciona mais ou menos como um sistema de cache</remarks>
        /// <param name="name">O nome da textura</param>
        /// <param name="temp">A textura achada, caso o nome exista</param>
        /// <returns>Se achou ou n�o a textura</returns>
        private static bool preLoadedTexture(string name, out Texture2D temp)
        {
            temp = null;

            KeyValuePair<Texture2D, int> pair;

            bool ok = (mDicLoadedResource.TryGetValue(name, out pair));
            if(ok)
            {
                temp = pair.Key;
                pair = new KeyValuePair<Texture2D, int>(temp, pair.Value + 1);
                mDicLoadedResource[name] = pair;
                return true;
            }else
            {
                return false;
            }
        }

        /// <summary>
        /// Insere uma textura dentro do sistema de cache
        /// </summary>
        /// <param name="name"></param>
        /// <param name="texture"></param>
        private static void insertTextureLoaded(string name, Texture2D texture)
        {
            KeyValuePair<Texture2D, int> pair;
            if (mDicLoadedResource.TryGetValue(name, out pair))
            {
                pair = new KeyValuePair<Texture2D, int>(texture, pair.Value + 1);
                mDicLoadedResource[name] = pair;
            }
            else
            {
                pair = new KeyValuePair<Texture2D, int>(texture, 1);
                mDicLoadedResource[name] = pair;

            }
        }

        /// <summary>
        /// Retorna uma textura 2D
        /// </summary>
        /// <param name="name">O nome do recurso</param>
        /// <param name="transparentColor">a cor transparente</param>
        /// <returns></returns>
        static public Texture2D getTexture2D(string name, Color transparentColor)
        {
            Texture2D temp = null;

            if (preLoadedTexture(name, out temp))
            {
                return temp;
            }

            Microsoft.Xna.Framework.Content.ContentManager cm = RPG.me.Content;
            temp = cm.Load<Texture2D>(@name);
            setAlphaColor(temp, transparentColor);
            insertTextureLoaded(name, temp);
            return temp;
        }

        /// <summary>
        /// Retorna uma textura considerando a cor no pixel (0,0) como cor de transpar�ncia
        /// </summary>
        /// <param name="name">O nome da textura buscada</param>
        /// <returns>A textura encontrada. null caso falha</returns>
        static public Texture2D getTexture2DFirstColorTransparent(string name)
        {
            Texture2D texture = null;

            if (preLoadedTexture(name, out texture))
            {
                return texture;
            }

            Microsoft.Xna.Framework.Content.ContentManager cm = RPG.me.Content;
            texture = cm.Load<Texture2D>(@name);

            {
                int size = (texture.Width * texture.Height);

                Color[] data = new Color[size];

                texture.GetData<Color>(data, 0, size);
                Color transparentPrev = data[0];
                Color transparentNew = Resource.TransparentColor(transparentPrev);

                for (int pos = 0; pos < size; pos++)
                {
                    if (data[pos] == transparentPrev)
                        data[pos] = transparentNew;
                }

                texture.SetData<Color>(data);
            }
            insertTextureLoaded(name, texture);
            return texture;
        }

        /// <summary>
        /// Retorna uma textura dado um nome associado
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        static public Texture2D getTexture2D(string name)
        {
            return getTexture2D(name, DEFAULT_TRANSPARENT_COLOR);
        }

        #endregion



        #region XML Manager

        /// <summary>
        /// Carrega um fluxo de XML baseado no nome do arquivo especificado
        /// </summary>
        /// <param name="str">nome do arquivo XML a ser carregado</param>
        /// <returns>Um XmlReader</returns>
        internal static System.Xml.XmlReader getXML(string str)
        {
            return new XmlTextReader(RPG.me.Content.RootDirectory + PATH_XML + str + EXT_XML);
        }

        /// <summary>
        /// Carrega um fluxo XML que representa um mapa contido na pasta de mapas usando o 'name' como nome do XML
        /// </summary>
        /// <param name="name">O nome do mapa XML buscado</param>
        /// <returns>O fluxo XML</returns>
        internal static System.Xml.XmlReader getXMLMap(string name)
        {
            return new XmlTextReader(RPG.me.Content.RootDirectory + PATH_MAP + name + EXT_MAP);
        }


        /// <summary>
        /// Cria um fluxo XML de grava��o
        /// </summary>
        /// <param name="name">nome do fluxo XML</param>
        /// <returns>O Fluxo gerado</returns>
        internal static System.Xml.XmlWriter createXML(string name)
        {
            return new XmlTextWriter(RPG.me.Content.RootDirectory + @"\xml\" + name + ".xml", Encoding.Default);
        }
        #endregion


#region Font Manager
        static public SpriteFont getFont(string name)
        {
            string temp = (PATH_FONT+name);
            return RPG.me.Content.Load<SpriteFont>(@temp);
        }
#endregion

        #region Color Util

        /// <summary>
        /// Gera uma cor com total tranpar�ncia
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        static private Color TransparentColor(Color color)
        {
            return new Color(color.R, color.G, color.B, 0);

        }

        /// <summary>
        /// Altera uma cor dentro de uma  textura para a cor transparente.
        /// </summary>
        /// <param name="texture">A Textura que sofrer� a substitui��o</param>
        /// <param name="color">A cor que ser� substituida por sua vers�o transparente</param>
        static public void setAlphaColor(Texture2D texture, Color color)
        {
            Resource.replaceColor(texture, color, TransparentColor(color));
        }

        /// <summary>
        /// Substitui v�rias cores por outras cores
        /// </summary>
        /// <remarks>
        /// Dada n cores de origem, o sistema substitui a i cor de origem pela i cor de destino.
        /// </remarks>
        /// <param name="texture">Textura que sofrer� a substitui��o</param>
        /// <param name="source">As cores de origem</param>
        /// <param name="destiny">A cor de destino</param>
        static public void replaceColor(Texture2D texture, Color[] source, Color[] destiny)
        {
            if (source.Length != destiny.Length) return;

            int size = (texture.Width * texture.Height);

            Color[] data = new Color[size];

            texture.GetData<Color>(data, 0, size);

            for (int pos = 0; pos < size; pos++)
            {
                for (int i = 0; i < source.Length; i++)
                {
                    if (data[pos] == source[i])
                        data[pos] = destiny[i];
                }
            }

            texture.SetData<Color>(data);
        }


        /// <summary>
        /// Substitui uma cor por outra
        /// </summary>
        /// <param name="texture">Textura que sofrer� a susbstitui��o</param>
        /// <param name="source">Cor de origem, que ser� substituida</param>
        /// <param name="destiny">Cor de destino</param>
        static public void replaceColor(Texture2D texture, Color source, Color destiny)
        {
            if (source != destiny) return;

            int size = (texture.Width * texture.Height);

            Color[] data = new Color[size];

            texture.GetData<Color>(data, 0, size);

            for (int pos = 0; pos < size; pos++)
            {
               if (data[pos] == source)
                   data[pos] = destiny;
            }

            texture.SetData<Color>(data);
        }
        #endregion
    }
}
