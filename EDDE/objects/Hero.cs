using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using edde.components;

namespace edde.objects
{
    /// <summary>
    /// GameObject que representa um personagem movimentável no jogo.
    /// </summary>
    public class Hero : GameObject
    {
        /// <summary>
        /// Um CharSet a ser exibido
        /// </summary>
        [Injection]
        private CharSetComponent mDrawSprite;

        [Injection]
        private ControlComponent mControl;

        public Hero():this(CharSet.PATTERN.PATTERN_DEFAULT)
        { }
        public Hero(CharSet.PATTERN pattern)
            : base()
        {
            mDrawSprite = new CharSetComponent("amanda", pattern);
            mControl = new ControlComponent();
            moving = true;
        }

        public string pattern
        {
            set { mDrawSprite.setStrPattern(value); }
        }



        public override bool init(Scene scene)
        {
            base.init(scene);
            return true;
        }

        public override void draw(GameTime time, ref Rectangle rectWindow)
        {
            base.draw(time, ref rectWindow);
        }


        public override void update(GameTime time)
        {
            base.update(time);

        }

      
        public override sealed bool testVision(GameTime gameTime)
        {
            return true;
        }
        public override sealed void verifyVisionContainer(ICollection<edde.physicalmanager.ICollised> collVision)
        {
            foreach (edde.physicalmanager.ICollised go in collVision)
            {
                Vector2 vecDistance = this.position - go.getRectBase().getCenter();

                double distance = vecDistance.Length();
            }
        }
    }


    
}
