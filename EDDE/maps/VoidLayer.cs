using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace edde.maps
{
    class VoidLayer: ILayer
    {
        public VoidLayer()
        { 
        }

        #region Layer Members

        public void draw(Rectangle rect, GameTime gameTime)
        {
           //nada
        }

        public void draw(Rectangle rect, ICollection<GameObject> coll, GameTime gameTime)
        {
            foreach (GameObject go in coll)
            {
                go.draw(gameTime, ref rect);
            }
        }

        #endregion
    }
}
