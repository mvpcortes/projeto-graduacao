using System.Collections.Generic;
using System;
using edde;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using edde.physicalmanager;

namespace edde.maps
{
    /// <summary>
    /// Implementa um Mapa 2D para ser pecorrido durante o jogo.
    /// <remarks>
    /// Este mapa possui v�rias propriedades especiais, entre elas as v�rias camadas, conter GameObjects e SoftComponents, entre outras.
    /// </remarks>
    /// </summary>
    public class Map: Scene
    {

        /// <summary>
        /// N�mero de agentes m�nimo para usar heur�stica de 
        /// </summary>
        private const int MIN_OBJECT_VISION_HEURISTIC = 100;

        private  int ZOrderComparison(GameObject a, GameObject b)
        {
            if (ReferenceEquals(a, b)) return 0;
            return MyMath.round(a.position.Y - b.position.Y);
        }

        /// <summary>
        /// Gerenciador de colis�o. Isto servir� principalmente para implementrat
        /// </summary>
        private IPhysicalManager mCollisionManager;

        /// <summary>
        /// retorna o CollisionManager associado
        /// </summary>
        public IPhysicalManager collisionManager
        {
            get { return mCollisionManager; }
        }
    	 
        /// <summary>
        /// A c�mera que especifica a janela de visualiza��o
        /// </summary>
	    private Camera mCamera;

        /// <summary>
        /// Cubo contendo as camadas de mapas e quais tilesets eles representam.
        /// </summary>
        private ILayer[] m_vecLayers;

        /// <summary>
        /// A largura do mapa
        /// </summary>
        private int mWidth;

        /// <summary>
        /// A altura do mapa
        /// </summary>
        private int mHeight;

        /// <summary>
        /// Walls que especificam as paredes do map
        /// </summary>
        private List<Wall> m_vecRoomWall;

       // private Texture2D mA;
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="rPG">O RPG que possui o mapa</param>
        /// <param name="name">O nome do mapa a ser criado</param>
        /// <param name="name">O tileset a ser usado</param>
        public Map(RPG rPG, string name, string tileset):base(rPG, name)
        {
            m_vecRoomWall   = new List<Wall>(4);
            m_vecLayers     = new maps.ILayer[(int)LAYER_ID.LENGTH];
            for (int i = (int)LAYER_ID.NONE + 1; i < (int)LAYER_ID.LENGTH; i++ )
            {
                m_vecLayers[i] = new VoidLayer();
            }
            
            //mWidth          = RPG.me.screenWidth() / mTileSet.getTileWidth();
            //mHeight         = RPG.me.screenHeight() / mTileSet.getTileHeight();


            //mCollisionManager = new SimpleCollision(this.getGameObjects(), this.getWalls());
            mCollisionManager = new CellCollisionManager(width, height);//este deve ser depois do TileSet
            // mCollisionManager = new QuadTreeNode(this.getWidth(), this.getHeight());//este deve ser depois do TileSet
            mCamera           = new Camera(this);
        }

        public Map(RPG rPG, string name)
            : this(rPG, name, name)
        {}

        
        
        public void setSize(int w, int h)
        {
            mWidth = w;
            mHeight = h;

            mCollisionManager.setSizeRegion(width, height);

            foreach (Wall wall in m_vecRoomWall)
            {
                mCollisionManager.removeObject(wall);
            }

            m_vecRoomWall.Clear();
            RectFloat temp = new RectFloat(0, 0, width, height);
            Wall.insertRoomWalls(m_vecRoomWall, temp );

            foreach (Wall wall in m_vecRoomWall)
            {
                mCollisionManager.addObject(wall);
            }
        }

        
        /// <summary>
        /// Retona a largura do mapa.
        /// </summary>
        /// <returns></returns>
        public int width { get { return mWidth; } }

        /// <summary>
        /// Retorna a altura do mapa.
        /// </summary>
        /// <returns></returns>
        public int height { get { return mHeight; } }

        public void setLayer(LAYER_ID id, ILayer layer)
        {
            ICollised oldCollised = m_vecLayers[(int)id] as ICollised;
            if(oldCollised != null)
                collisionManager.removeObject(oldCollised);

            m_vecLayers[(int)id] = layer;

            ICollised collised = m_vecLayers[(int)id] as ICollised;            
            if(collised != null)
                collisionManager.addObject(collised);

        }

        public ILayer floorLayer    { get { return m_vecLayers[(int)LAYER_ID.FLOOR];    }   set { setLayer(LAYER_ID.FLOOR, value);    } }
        public ILayer roomLayer     { get { return m_vecLayers[(int)LAYER_ID.ROOM];     }   set { setLayer(LAYER_ID.ROOM, value);     } }
        public ILayer upRoomLayer   { get { return m_vecLayers[(int)LAYER_ID.UPROOM];   }   set { setLayer(LAYER_ID.UPROOM, value);   } }
    
        #region Screne Members
        private static IList<ICollised> msListCollisionGameObject = new List<ICollised>();//s�o listas array pq haver� pouca modificia��o do conte�do dos gameObjects
        private static IList<ICollised> msListTouchGameObject = new List<ICollised>();
        private static IList<ICollised> msListVision = new List<ICollised>();
        public override void Update(GameTime gameTime)
        {
             foreach (GameObject go in getGameObjects())
            {
                if (go.testVision(gameTime))
                {
                    msListVision.Clear();
                //    if (getGameObjects().Count + m_vecRoomWall.Count < MIN_OBJECT_VISION_HEURISTIC)
                //        testVision(go, 200f, msListVision);
                //    else
                        mCollisionManager.testVision(go, msListVision);

                    go.verifyVisionContainer(msListVision);
                }
                mCollisionManager.removeObject(go);
                go.update(gameTime);
                mCollisionManager.addObject(go);

                mCollisionManager.testColision(go, msListCollisionGameObject, msListTouchGameObject);

                if (go.onColision(msListCollisionGameObject))
                {
                    mCollisionManager.removeObject(go);
                    go.restorePrevPosition();
                    mCollisionManager.addObject(go);
                }
                
                go.onTouch(msListTouchGameObject);

                go.backupPosition();

                msListCollisionGameObject.Clear();
                msListTouchGameObject.Clear();
            }

            msListCollisionGameObject.Clear();
            msListTouchGameObject.Clear();
            
            //atualiza a ordem do "zbuffer"
            getGameObjects().Sort(ZOrderComparison);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch sb = RPG.me.spriteBatch;
            //descobre o offset
            Rectangle rect = mCamera.processRect();

            floorLayer.draw (rect, gameTime);
            roomLayer.draw  (rect, getGameObjects(), gameTime);
            //agora os objetos
            /*List<GameObject> listTemp = getGameObjects();
        
            RectFloat rectFloat = RectFloat.FromRectangle(rect);
            foreach(GameObject go in getGameObjects())
            {
               go.draw(gameTime, new Point(rect.Left, rect.Top));
            }*/

          /* // Texture2D texture = Resource.getTexture2DFirstColorTransparent("tileset\\audia");
            foreach (Wall wall in getWalls())
            {
                Rectangle rectWall = wall.getRectBase().toRectangle();
                rectWall.X -= rect.Left;
                rectWall.Y -= rect.Top;
                sb.Draw(mTileSet.getTexture(), rectWall, mTileSet.getRect(10), Color.White);
            }
           */

            upRoomLayer.draw(rect, gameTime);

        }

        public override void addObject(GameObject obj)
        {
            base.addObject(obj);
            mCollisionManager.addObject(obj);
        }
        #endregion

        public void addWall(Wall wall)
        {
            m_vecRoomWall.Add(wall);
            mCollisionManager.addObject(wall);
        }

        public ICollection<Wall> getWalls()
        {
            return m_vecRoomWall;
        }

        public void followObject(GameObject obj)
        {
            mCamera.followIt(obj);
        }

       public void importStringData(LAYER_ID id, string data)
       {
           if(!(m_vecLayers[(int)id] is TileSetLayer))
               m_vecLayers[(int)id] = new TileSetLayer("audia", 30, 30);

            TileSetLayer tsl = (TileSetLayer)m_vecLayers[(int)id];

            tsl.importStringData(data);
       }

        private bool testVision(GameObject obj, ICollised other, float distVision)
        {
            Vector2 vecDist = other.getRectBase().getCenter() - obj.position;
            if (vecDist.Length() < distVision)
            {
                Vector2 vecVision = obj.getDirection();
                vecVision.Normalize();
                vecDist.Normalize();
                float angle = (float)Math.Acos((double)Vector2.Dot(vecDist, vecVision));
                if (angle < Collision.VISION_ANGLE)
                    return true;
            }
            return false;
        }

        private void testVision(GameObject obj, float distVision, ICollection<ICollised> collVisible)
        {
            collVisible.Clear();

            foreach(ICollised collised in getGameObjects())
            {
                if (!ReferenceEquals(collised, obj))
                {
                    if (testVision(obj, collised, distVision))
                        collVisible.Add(collised);                    
                }
            }

            foreach (ICollised collised in m_vecRoomWall)
            {
                if (testVision(obj, collised, distVision))
                        collVisible.Add(collised);
            }
        }


        public override sealed void removeGameObject(GameObject go)
        {
            base.removeGameObject(go);
            mCollisionManager.removeObject(go);
            
        }

        public override void getGameObjectInRegion(Vector2 center, float range, ICollection<ICollised> collItems)
        {
            this.mCollisionManager.getObjectRegion(center, range, collItems);
        }
    }
}