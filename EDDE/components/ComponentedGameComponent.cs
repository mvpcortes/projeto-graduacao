﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace edde
{
    /// <summary>
    /// Isto é um componente que permite inserir componentes dentro dele
    /// </summary>
    public class ComponentedGameComponent : DrawableGameComponent, SoftComponentContainer
    {
        /// <summary>
        /// É preciso criar um GameObject para conter os componentes que o compõe
        /// </summary>
        GameObject mGameObjectStub;

        SpriteBatch mSpriteBatch = null;

        #region SoftComponentContainer Members

        public bool addComponent(SoftComponent comp)
        {
            return mGameObjectStub.addComponent(comp);
        }

        public bool removeComponent(SoftComponent comp)
        {
            return mGameObjectStub.removeComponent(comp);
        }

        public int removeComponent(Type className)
        {
            return mGameObjectStub.removeComponent(className);
        }

        public SoftComponent getComponent(Type className)
        {
            return mGameObjectStub.getComponent(className);
        }

        public int getComponent(Type className, ICollection<SoftComponent> container)
        {
            return mGameObjectStub.getComponent(className, container);
        }

        public bool referenceComponent(SoftComponent component)
        {
            return mGameObjectStub.referenceComponent(component);
        }

        #endregion

        public ComponentedGameComponent(RPG rpg):base(rpg)
        {
            mGameObjectStub = new GameObject();
        }

        public override void  Initialize()
        {
            GameObject.findInjectedItems(mGameObjectStub, this);
            mGameObjectStub.init(null);
            mSpriteBatch = RPG.me.spriteBatch;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            mSpriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.None);
            Rectangle rect = new Rectangle(0, 0, RPG.me.screenWidth, RPG.me.screenHeight);
            mGameObjectStub.draw(gameTime, ref rect);
            mSpriteBatch.End();
        }
    }
}
