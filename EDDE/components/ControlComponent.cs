using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using edde.physicalmanager;

namespace edde.components
{
    public class ControlComponent: SoftComponent
    {
        public delegate void ListenControl(InputControl input);

        private edde.InputControl mInputControl;

        private GameObject mParent;

        private Vector2 mIncMove;

        private bool mEnable = true;

        public bool enable
        {
            get { return mEnable; }
            set { mEnable = value; }
        }

        public Vector2 incValue
        {
            get { return mIncMove; }
            set { mIncMove = value; }
        }
        public ControlComponent():base()
        {
            mIncMove = new Vector2(1f, 1f);
        }

        public event ListenControl OnUpdateControl;

        #region SoftComponent Members

        public void draw(Microsoft.Xna.Framework.GameTime time, ref Rectangle rectWindow)
        {
            //NOTHING
        }

        public void  update(Microsoft.Xna.Framework.GameTime time)
        {
            if (!enable) return;     
         
            float x = 0f, y = 0f;
            if (mInputControl.isKeyPress(InputControl.RPG_KEY.LEFT))
            {
                x -= mIncMove.X;
            }
            if (mInputControl.isKeyPress(InputControl.RPG_KEY.RIGHT))
            {
                x += mIncMove.X;
            }

            if (mInputControl.isKeyPress(InputControl.RPG_KEY.UP))
            {
                y -= mIncMove.Y;
            }
            if (mInputControl.isKeyPress(InputControl.RPG_KEY.DOWN))
            {
                y += mIncMove.Y;
            }
            if (x > 0 && y > 0 && mInputControl.isKeyPress(InputControl.RPG_KEY.ACTION_A))
            {
            }

            if (!mInputControl.isKeyPress(InputControl.RPG_KEY.ACTION_L))
                mParent.setTypeCollision(COLLISION_TYPE.SOLID);
            else
            {
                //RPG.me.mSoundManager.Play("lanlanlan");
                mParent.setTypeCollision(COLLISION_TYPE.VOID);
            }

            if (!(x == 0f && y == 0f))
            {
                mParent.setDirection(new Vector2(x, y));
                mParent.moving = (true);
            }
            else
                mParent.moving = (false);


            mParent.incPosition(x ,y);

            if (OnUpdateControl != null)
                OnUpdateControl(mInputControl);
        }
        public bool init(GameObject parent)
        {
            mParent = parent;

            mInputControl = RPG.me.inputControl;
            return !ReferenceEquals(mParent, null);
        }

        public void  ejected()
        {            
        }

        public bool  ejectMe()
        {
            return false;
        }

        #endregion
    }

}
