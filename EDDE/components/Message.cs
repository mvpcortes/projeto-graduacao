using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace edde.components
{
   public class Message: SoftComponent
    {  
       public enum STATE_MESSAGE
       { 
           SHOW,
           MESSAGING,
           HIDE,
           EJECT_ME
       }

       string mMessage;

       GameObject mGameObjectSource = null;
       private bool mEnabled         = false;
       Vector2 mVecSource           = Vector2.Zero;
       TimeSpan mTimeFinal, mTimeInit;
       Vector2 mVelocity;
       Vector2 mVecActual;
       Texture2D mTextureBackGround;
       float mScale;
       STATE_MESSAGE mState;

       public Message(string message, Vector2 vecSource, Vector2 vecDestiny, TimeSpan time)
       {
           mTextureBackGround = Resource.getTexture2DFirstColorTransparent("frame\\system");
           mMessage = message;
           mGameObjectSource = null;
           mState = STATE_MESSAGE.SHOW;

           mVelocity = (vecDestiny - vecSource) / (float)(time.TotalMilliseconds);
           mVecSource = vecSource;
           mTimeInit = TimeSpan.Zero;
           mTimeFinal = time;
       }

        #region SoftComponent Members

        public void draw(Microsoft.Xna.Framework.GameTime time, ref Rectangle rectWindow)
        {
            SpriteBatch sb = RPG.me.spriteBatch;
            sb.Draw(mTextureBackGround, mVecActual, null, Color.White, 0, Vector2.Zero, mScale, SpriteEffects.None, 0);
        }

        public void update(Microsoft.Xna.Framework.GameTime time)
        {
            TimeSpan actualTime = time.TotalGameTime;
            switch (mState)
            { 
                case STATE_MESSAGE.SHOW:
                    if (mTimeInit == TimeSpan.Zero)
                    {
                        mTimeInit = actualTime ;
                        mTimeFinal += mTimeInit;
                    }

                    if (mTimeFinal < actualTime)
                    {
                        mState = STATE_MESSAGE.MESSAGING;
                        mVecActual = mVecSource + mVelocity * (float)(mTimeFinal - mTimeInit).TotalMilliseconds;
                        mScale = 1;
                    }
                    else
                    {
                        mVecActual = mVecSource + mVelocity * (float)(actualTime - mTimeInit).TotalMilliseconds;
                        mScale = (float)((actualTime - mTimeInit).TotalMilliseconds / (mTimeFinal - mTimeInit).TotalMilliseconds);
                    }

                break;
                case STATE_MESSAGE.MESSAGING:
                    {
                        mScale = 1;
                        mState = STATE_MESSAGE.HIDE;
                        mTimeFinal = actualTime + (mTimeFinal - mTimeInit);
                        mTimeInit = actualTime;
                        mVecSource = mVecActual;
                        mVelocity = mVelocity * -1f;
                    }

                break;
                case STATE_MESSAGE.HIDE:
                    if (mTimeFinal < actualTime)
                    {
                        mVecActual = mVecSource + mVelocity * (float)(mTimeFinal - mTimeInit).TotalMilliseconds;
                        mState = STATE_MESSAGE.EJECT_ME;
                        mScale = 0;
                    }
                    else
                    {
                        mVecActual = mVecSource + mVelocity * (float)(actualTime - mTimeInit).TotalMilliseconds;
                        mScale = 1- (float)((actualTime - mTimeInit).TotalMilliseconds / (mTimeFinal - mTimeInit).TotalMilliseconds);
                    }


                break;
            }
        }

        public bool init(GameObject parent)
        {
            mGameObjectSource = parent;
            return true;
        }

        public void ejected()
        {
            //throw new Exception("The method or operation is not implemented.");
        }


        public bool ejectMe()
        {
            return mState == STATE_MESSAGE.EJECT_ME;
        }

       public bool enable
       {
           get { return mEnabled; }
           set { mEnabled = true; }
       }

        #endregion
    }
}
