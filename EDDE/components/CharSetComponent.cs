using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using edde.physicalmanager;

namespace edde.components
{
    /// <summary>
    /// SoftComponent que exibe um charset para um GameObject.
    /// </summary>
    public class CharSetComponent: CharSet, SoftComponent
    {
        /// <summary>
        /// Modos de anima��o de um CharSet
        /// </summary>
        public enum ANIMATED_STATE
        {
            NONE = -1,
            FIX,            ///sem anima��o. Exibe o frame especificado em mnFrame
            MOVED,          ///animado durante movimento. Verifica se o pai est� movendo, se sim, faz movimento.
            ANIMATED,       ///animado. Sempre se mover�.
            LENGTH
        }

        /// <summary>
        /// Velocidades padr�es de anima��o
        /// </summary>
        public static class SPEED
        {
            public static TimeSpan VERY_SLOW    = new TimeSpan(0, 0, 5);
            public static TimeSpan SLOW         = new TimeSpan(0, 0, 1);
            public static TimeSpan NORMAL       = new TimeSpan(0, 0, 0, 0, 500);
            public static TimeSpan FAST         = new TimeSpan(0, 0, 0, 0, 250);
            public static TimeSpan VERY_FAST    = new TimeSpan(0, 0, 0, 0, 100);
        }

        #region attributes
        /// <summary>
        /// Indica em qual frame est� o charset atualmente. Este valor � uma posi��o no vetor de anima��o mPattern.VEC_ANIMATION. Isto �, o frame real � igual a mFrameSeq[mnFrame].
        /// </summary>
        private int mnFrame;

        /// <summary>
        /// tempo de exibi��o de um frame, em segundos.
        /// </summary>
        private TimeSpan mTimeFrame;

        /// <summary>
        /// �ltimo tempo de altera��o do frame. Quando o tempo atual para este tempo dista mTimeFrame, � hora de trocar o frame.
        /// </summary>
        private TimeSpan mTimeLastFrame;

        ///<summary>
        ///ponto do charset onde se localizar� o ponto central do GameObject.
        ///</summary>
        ///<remarks>
        /// Para exibir um charset em um gameObject, o sistema pega o ponto central do GameObject e o liga ao ponto entral do CharSet.
        ///</remarks>
        private Point mPointCenter;         
       
        /// <summary>
        /// Especifica qual � o modo de anima��o. Fixo, em movimento do personagem ou sempre em anima��o
        /// </summary>
        private ANIMATED_STATE mAnimatedState;

        /// <summary>
        /// Refer�ncia ao GameObject que possui este componente
        /// </summary>
        protected GameObject mParent;

        /// <summary>
        /// Cor de sombra usada na pintura do item
        /// </summary>
        private Color mShandowColor = Color.White;

        /// <summary>
        /// habilitar ou n�o o componente
        /// </summary>
        private bool mEnabled = true;

        /// <summary>
        /// Dire��o usada no lugar da dire��o do parent. S� ir� funcionar se for diferente de Vector2.Zero.
        /// </summary>
        private Vector2 mSelfDirection = Vector2.Zero;
        #endregion


        #region Properties
        public Vector2 selfDirection
        {
            get { return mSelfDirection; }
            set
            {
                mSelfDirection = value;
                mSelfDirection.Normalize();
            }
        }
        private void setParentDirection()
        {
            mSelfDirection = Vector2.Zero;
        }

        private bool isParentDirection()
        {
            return mSelfDirection == Vector2.Zero;
        }
        #endregion

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="nameResource">nome do recurso usado para se criar o charset</param>
        public CharSetComponent(string nameResource) : this(nameResource, PATTERN.PATTERN_DEFAULT)
        {}

        public CharSetComponent(string nameResource, PATTERN pattern): base(nameResource, pattern)
        {
            mnFrame = mPattern.MIDDLE_FRAME;

            mTimeFrame = SPEED.FAST;

            mTimeLastFrame = TimeSpan.Zero;

            mAnimatedState = ANIMATED_STATE.MOVED;

            mPointCenter = new Point(12, 24);
        }

        #region Set/Get

        public Color colorShandow
        {
            get { return mShandowColor; }
            set { mShandowColor = value; }
        }

        /// <summary>
        /// Atribui
        /// </summary>
        /// <param name="value">valor do frame relativo</param>
        public void setRelativeFrame(int value)
        {
            mnFrame = value % mPattern.VEC_ANIMATION.Length;
        }
        
        /// <summary>
        /// Incrementa o frame relativo.
        /// </summary>
        /// <param name="value">valor de incremento. Se o valor destino estrapolar o n�mero m�ximo de itens no vetor de anima��o, o valor � setado como m�dulo do tamanho m�ximo do vetor de anima��o</param>
        protected void incRelativeFrame(int value)
        {
            mnFrame += value;
            mnFrame = mnFrame % mPattern.VEC_ANIMATION.Length;
        }


        /// <summary>
        /// Retorna o frame real a ser exibido
        /// </summary>
        /// <returns>o frame real sendo exibido</returns>
        protected int getFrame()
        {
            return mPattern.VEC_ANIMATION[mnFrame];
        }

        /// <summary>
        /// retorna o frame relativo sendo exibido
        /// </summary>
        /// <returns></returns>
        private int getRelativeFrame() 
        {
            return mnFrame;
        }

        /// <summary>
        /// Atribui um tempo de exibi��o de um frame
        /// </summary>
        /// <param name="time">O tempo de exibi��o de um frame</param>
        public void      setTimeFrame(TimeSpan time)
        {
            if (time == TimeSpan.Zero)
                mTimeFrame = SPEED.VERY_FAST;
            else
                mTimeFrame = time; 
        }

        /// <summary>
        /// Retorna o tempo de exibi��o de um frame
        /// </summary>
        /// <returns></returns>
        public  TimeSpan getTimeFrame()              { return mTimeFrame; }

        /// <summary>
        /// Retorna o modo de anima��o do charset
        /// </summary>
        /// <seealso cref="ANIMATED_STATE"/>
        /// <returns></returns>
        public ANIMATED_STATE getAnimatedState() { return mAnimatedState; }


        /// <summary>
        /// Atribui um modo de anima��o
        /// </summary>
        /// <seealso cref="ANIMATED_STATE"/>
        /// <param name="an_state">o modo de anima��o a ser atribu�do</param>
        public void setAnimatedState(ANIMATED_STATE an_state)
        {
            mAnimatedState = an_state;
            
            //for para o modo animado ou movendo, ele especifica o frame default.
            if (an_state == ANIMATED_STATE.ANIMATED || an_state == ANIMATED_STATE.MOVED)
            {
                mnFrame = getMiddleFramePosition();
            }
        }

        public override void setStrPattern(string name)
        {
            base.setStrPattern(name);
            mnFrame = mPattern.MIDDLE_FRAME;
            if (!ReferenceEquals(mParent, null))
                mParent.setRectBase(mPattern.RECT_BASE);
        }

        public override void setPattern(PATTERN pattern)
        {
            base.setPattern(pattern);
            mnFrame = pattern.MIDDLE_FRAME;
            if (!ReferenceEquals(mParent, null))
                mParent.setRectBase(pattern.RECT_BASE);
        }

        public Point centerPoint
        {
            get { return mPointCenter; }
            set { mPointCenter = value; }
        }

        #endregion


        private int getMiddleFramePosition()
        {
            for (int i = 0; i < mPattern.VEC_ANIMATION.Length; i++)
            {
                if (mPattern.VEC_ANIMATION[i] == mPattern.MIDDLE_FRAME)
                    return i;
            }

            return 0;
        }

        /*
        private void updateCoord(Coords coord)
        {
            this.setRelativeFrame(coord.frame);
            this.setCharSet(coord.posX, coord.posY);
        }
         */

        #region SoftComponent Members

        public virtual void draw(GameTime time, ref Rectangle rectWindow)
        {
            RectFloat rectDest = new RectFloat((mParent.position - new Vector2(centerPoint.X, centerPoint.Y)), new Vector2(getTileWidth(), getTileHeight()));

             if (!Collision.intersects(rectDest, RectFloat.FromRectangle(rectWindow))) return;

            rectDest.top -= rectWindow.Y;
            rectDest.left -= rectWindow.X;

            Rectangle rectSource = getCharSetRect(isParentDirection()?mParent.getDirection():mSelfDirection, getFrame());

            SpriteBatch sb = RPG.me.spriteBatch;
            sb.Draw(mTexture, rectDest.toRectangle(), rectSource, colorShandow);  
        }

        public virtual void update(GameTime time)
        {
            TimeSpan actualTime = time.TotalGameTime;

            switch (getAnimatedState())
            { 
                case ANIMATED_STATE.FIX:
                    //frame = getFrame();
                    //faznada
                break;
                case ANIMATED_STATE.MOVED:
                case ANIMATED_STATE.ANIMATED:
               //     if (!ReferenceEquals(OnEndFrame, null))
                  //      if(OnEndFrame(ref coord)) updateCoord(coord);
                    if (mParent.moving || getAnimatedState() == ANIMATED_STATE.ANIMATED )
                    {
                        TimeSpan dif = actualTime - mTimeLastFrame;
                        if (dif > getTimeFrame())
                        {
                            int frameSpent = 0;
                            frameSpent = (int)(dif.Ticks / getTimeFrame().Ticks);

                            mTimeLastFrame = actualTime;
                            incRelativeFrame(frameSpent);
                        }
                    }
                    else
                    {
                        mnFrame = mPattern.MIDDLE_FRAME;
                    }
              //      coord.frame = mnFrame;
                break;
            }
     }
        public virtual bool init(GameObject parent)
        {
            mParent = parent;
            return true;
        }

        public void ejected()
        {
        }

        public bool ejectMe()
        {
            return false;
        }
        public bool enable
        {
            get { return mEnabled; }
            set { mEnabled = value; }
        }

        #endregion
    }
}
