using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using util;
using edde.physicalmanager;

namespace edde.components
{

    public class GameGridComponent : SoftComponent
    {
        /// <summary>
        /// Classe que representa uma c�lula ga grid
        /// </summary>
        public class Cell
        {
            #region fields
            /// <summary>
            /// Texto da grid
            /// </summary>
            public string mText;

            /// <summary>
            /// Cor da fonte
            /// </summary>
            public Color mFontColor;

            /// <summary>
            /// Cor de fundo
            /// </summary>
            public Color mBgColor;

            /// <summary>
            /// Usando cor de fundo
            /// </summary>
            public bool mUsingBgColor;

            #endregion

            public string text
            {
                get{return mText;}
                set{mText = value;}
            }
            /// <summary>
            /// Construtor
            /// </summary>
            /// <param name="text">Texto da c�lula</param>
            /// <param name="fontColor">Cor da fonte</param>
            /// <param name="bgColor">Cor de fundo</param>
            public Cell(string text, Color fontColor, Color bgColor)
            {
                mText = text;
                mFontColor = Color.White;
                mBgColor = bgColor;
                mUsingBgColor = true;
            }

            public Cell(string text, Color fontColor)
            {
                mText = text;
                mFontColor = Color.White;
                mBgColor = Color.Black;
                mUsingBgColor = false;
            }

            public Cell(Cell other)
            {
                other.copyTo(this);
            }

            public Cell clone()
            {
                return new Cell(this);
            }

            public void copyTo(Cell other)
            {
                other.mText         = this.mText;
                other.mFontColor    = this.mFontColor;
                other.mBgColor      = this.mBgColor;
                other.mUsingBgColor = this.mUsingBgColor;
            }

            public static Cell GLOBAL_DEFAULT_CELL = new Cell("", Color.White);
        }

        #region constants
        public const int DEFAULT_ROW_HEIGHT = 16;
        public const int DEFAULT_COL_WIDTH  = 100;
        public const float DEFAULT_FONT_SIZE = 1;
        public static readonly Color DEFAULT_COLOR_LINE = Color.White;
        public const string DEFAULT_FONT = "arial_8";
        public static readonly Texture2D DEFAULT_BG_TEXTURE;
        
        static GameGridComponent()
        {
           Color[] data = new Color[1];
           Texture2D temp = new Texture2D(RPG.me.GraphicsDevice, 1, 1, 1, TextureUsage.None, SurfaceFormat.Color);
           temp.GetData<Color>(data);
           data[0] = Color.White;
           temp.SetData<Color>(data);
           DEFAULT_BG_TEXTURE = temp;
        }

        #endregion


        #region fields


        /// <summary>
        /// Habilitado ou n�o
        /// </summary>
        private bool mEnabled = true;

        /// <summary>
        /// C�lula default usada na constru��o das outras
        /// </summary>
        private Cell mDefaultCell;

        /// <summary>
        /// Lista com a altura das linhas
        /// </summary>
        private List<int> mListRowHeight;

        /// <summary>
        /// Lista com a largura das colunas
        /// </summary>
        private List<int> mListColWidth;


        /// <summary>
        /// somat�rio da largura das colunas = largura da Grid
        /// </summary>
        private int mnSumCol;

        /// <summary>
        /// Somat�rio da altura das linhas = altura da Grid
        /// </summary>
        private int mnSumRow;

        /// <summary>
        /// Matrix contendo as c�lulas
        /// </summary>
        private Matrix<Cell> mData;

        /// <summary>
        /// PrimitiveBatch para desenhar as linhas
        /// </summary>
        PrimitiveBatch mPrimitiveBatch;

        /// <summary>
        /// SpriteBatch para desenhar as fontes
        /// </summary>
        SpriteBatch mSpriteBatch;


        /// <summary>
        /// Fonte
        /// </summary>
        private SpriteFont mFont;

        private string mStrFont;

        /// <summary>
        /// Tamanho da fonte
        /// </summary>
        private float mnFontSize;

        /// <summary>
        /// Cor da linha
        /// </summary>
        private Color mColorLine;

        /// <summary>
        /// Textura do fundo da grid
        /// </summary>
        private Texture2D mTextBg;


        /// <summary>
        /// Referencia ao pai GameObject
        /// </summary>
        private GameObject mParent;


        /// <summary>
        /// Determina se o componente deve seguir o parent ou deve ser orientado pela tela
        /// </summary>
        private bool mFollowParent;


        /// <summary>
        /// Determina o vetor de deloscamento em rela��o ao Parent ou em rela��o a tela
        /// </summary>
        Point mPointRef;

        #endregion

        #region properties
        

        /// <summary>
        /// Cor da linha
        /// </summary>
        private Color lineColor
        {
            get { return mColorLine; }
            set { mColorLine = value; }
        }


        /// <summary>
        /// Textura de fundo
        /// </summary>
        private Texture2D bgTexture
        {
            set { mTextBg = value; }
        }

        /// <summary>
        /// c�lula default usada na constru��o das outras
        /// </summary>
        public Cell defaultCell
        {
            get { return mDefaultCell; }
            set { mDefaultCell = value; }
        }

        /// <summary>
        /// Ponto de refer�ncia em rela��o ao parent ou a tela
        /// </summary>
        public Point pointReference
        {
            get { return mPointRef; }
            set { mPointRef = value; }
        }
        
        /// <summary>
        /// Largura 
        /// </summary>
        public int width { get { return mnSumCol; } }

        /// <summary>
        /// Altura
        /// </summary>
        public int height { get { return mnSumRow; } }

        /// <summary>
        /// nome da fonte usada
        /// </summary>
        public string font
        {
            get { return mStrFont; }
            set {
                mStrFont = value; 
                mFont = Resource.getFont(value); 
                }
        }

        public float fontSize
        {
            get { return mnFontSize; }
            set { mnFontSize = value; }
        }

        /// <summary>
        /// Determina que a grid deve seguir o seu Parent;
        /// </summary>
        public bool followParent
        {
            get { return mFollowParent; }
            set { mFollowParent = value; }
        }

        /// <summary>
        /// Determina que a grid � referenciada pelo topo do Screen. � o inverso de followParent
        /// </summary>
        public bool fixScreen
        {
            get { return !mFollowParent; }
            set { mFollowParent = !value; }
        }


        public Cell this[int row, int col]
        {
            get
            {
                return mData[col, row];
            }
        }

        //public Cell getCell(int row, int col)
        //{

        //}

        public bool enable
        {
            get { return mEnabled; }
            set { mEnabled = value; }
        }

        #endregion

        public int getRowHeight(int row) { return mListRowHeight[row]; }
        public void setRowHeight(int row, int height)
        {
            int dif = height - mListRowHeight[row];
            mListRowHeight[row] = height;
            mnSumRow += dif;
        }

        public int getColWidth(int col) { return mListColWidth[col]; }
        public void setColWidth (int col, int width)
        {
            int dif = width - mListColWidth[col];
            mListColWidth[col] = width;
            mnSumCol += dif;
        }


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="rows">N�mero de linhas da Grid</param>
        /// <param name="cols">N�mero de colunas da Grid</param>
        public GameGridComponent(int rows, int cols)
        {
           mDefaultCell = Cell.GLOBAL_DEFAULT_CELL.clone();

           mListColWidth = new List<int>(cols); for (int a = 0; a < cols; a++) mListColWidth.Add(DEFAULT_COL_WIDTH);
           mListRowHeight = new List<int>(rows); for (int a = 0; a < rows; a++) mListRowHeight.Add(DEFAULT_ROW_HEIGHT);
           mData = new Matrix<Cell>(cols, rows);

           for (int i = 0; i < cols; i++)
               for (int j = 0; j < rows; j++)
               {
                   mData[i, j] = defaultCell.clone();
               }

           mTextBg = DEFAULT_BG_TEXTURE;

           mColorLine = DEFAULT_COLOR_LINE;

           mnFontSize = DEFAULT_FONT_SIZE;

           mEnabled = true;

           mFollowParent = true;

           updateSum();
        }

        /// <summary>
        /// Atrualiza os valores de soma da largura de colunas e da altura de linhas. Correspondem a altura e largura da Grid
        /// </summary>
        private void updateSum()
        {
            mnSumCol = mnSumRow = 0;
            foreach (int i in mListColWidth)
                mnSumCol += i;
            foreach (int i in mListRowHeight)
                mnSumRow += i;

        }

        public int colCount
        {
            get{return mListColWidth.Count;}
        }

        public int rowCount
        {
            get { return mListRowHeight.Count; }
        }
            
        /// <summary>
        /// Especifica o n�mero de linhas e colunas
        /// </summary>
        /// <param name="row">n�mero de linhas</param>
        /// <param name="col">n�mero de colunas</param>
        public void setSize(int rows, int cols)
        {
            mData.setSize(cols, rows, defaultCell);
            for (int row = 0; row < rows; row++)
                for (int col = 0; col < cols; col++)
                {
                    mData[col,row] = defaultCell.clone();
                }

            mListColWidth.Clear(); mListColWidth.Capacity = cols; for (int a = 0; a < cols; a++) mListColWidth.Add(DEFAULT_COL_WIDTH);
            mListRowHeight.Clear(); mListRowHeight.Capacity = rows; for (int a = 0; a < rows; a++) mListRowHeight.Add(DEFAULT_ROW_HEIGHT);
            updateSum();
        }
        #region SoftComponent Members


        /// <summary>
        /// inicia o componente
        /// </summary>
        /// <param name="parent">o Parent</param>
        /// <returns>Se foi poss�vel construir ou n�o</returns>
        public bool init(GameObject parent)
        {
            mPrimitiveBatch = new PrimitiveBatch(RPG.me.GraphicsDevice);

            mSpriteBatch = RPG.me.spriteBatch;

            font = DEFAULT_FONT;

            mParent = parent;

            Point desloc = Point.Zero;
            {
                CharSetComponent charSet = parent.getComponent(typeof(CharSetComponent)) as CharSetComponent;
                if(!ReferenceEquals(charSet, null))
                {
                    desloc.X = +charSet.getTileWidth()/2;
                    desloc.Y = -charSet.getTileHeight()/2;
                }
            }

            pointReference = new Point(desloc.X, desloc.Y);

            return true;
        }

        /// <summary>
        /// evento de eje��o do componente
        /// </summary>
        public void ejected()
        {
            //nothing
        }

        /// <summary>
        /// Verifica se o componente quer ser ejetado ou n�o.
        /// </summary>
        /// <returns>true se ele quer ser ejetado. O parent neste caso remover� ele</returns>
        public bool ejectMe()
        {

             return false;
        }


      

        /// <summary>
        /// Pinta o componente
        /// </summary>
        /// <param name="time"></param>
        /// <param name="offSet"></param>
        public void draw(GameTime time, ref Rectangle rectWindow)
        {
            Point pointSource = pointReference;
            if (followParent)
            {
                Vector2 vec = mParent.position;
                pointSource = new Point((int)vec.X + pointSource.X, (int)vec.Y + pointSource.Y);
            }

            if (!Collision.intersects(new RectFloat(pointSource.Y, pointSource.X, pointSource.X + width, pointSource.Y+height), RectFloat.FromRectangle(rectWindow)))
                return;

            pointSource.X -= rectWindow.X;
            pointSource.Y -= rectWindow.Y;

            Point point = pointSource;
            for (int row = 0; row < mData.Height; row++)
            {
                for (int col = 0; col < mData.Width; col++)
                {
                    mSpriteBatch.Draw(mTextBg, new Rectangle(point.X, point.Y, mListColWidth[col], mListRowHeight[row]), mData[col, row].mBgColor);
                    mSpriteBatch.DrawString(mFont, mData[col, row].mText, new Vector2(point.X, point.Y), mData[col, row].mFontColor, 0, Vector2.Zero, this.fontSize, SpriteEffects.None, 0);
                    point.X += mListColWidth[col];
                }
                point.Y += mListRowHeight[row];
                point.X = pointSource.X;
            }
            mSpriteBatch.End();
            mPrimitiveBatch.Begin(PrimitiveType.LineList);
            //
            point = pointSource;

            mPrimitiveBatch.AddVertex(new Vector2(point.X, point.Y), lineColor);
            mPrimitiveBatch.AddVertex(new Vector2(point.X, point.Y + mnSumRow), lineColor);

            for (int col = 0; col < mData.Width; col++)
            {
                point.X += mListColWidth[col];
                mPrimitiveBatch.AddVertex(new Vector2(point.X, point.Y), Color.White);
                mPrimitiveBatch.AddVertex(new Vector2(point.X, point.Y + height), lineColor);
            }

            //====================================================================================
            point = pointSource;

            mPrimitiveBatch.AddVertex(new Vector2(point.X, point.Y), Color.White);
            mPrimitiveBatch.AddVertex(new Vector2(point.X + width, point.Y), lineColor);
            for (int row = 0; row < mData.Height; row++)
            {
                point.Y += mListRowHeight[row];
                mPrimitiveBatch.AddVertex(new Vector2(point.X, point.Y), lineColor);
                mPrimitiveBatch.AddVertex(new Vector2(point.X + width, point.Y), lineColor);
            }
            mPrimitiveBatch.End();
            mSpriteBatch.Begin();
        }

        public void update(GameTime time)
        {
            //faznada 
        }

        #endregion
    }

}
