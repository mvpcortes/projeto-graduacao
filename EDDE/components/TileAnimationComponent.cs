﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace edde.components
{
    /// <summary>
    /// Classe que pinta um Tile de um arquivo
    /// <remarks>
    /// É uma forma mais simples do CharSetComponent. Não tem animação propriamente, mas permite alterar o tile exibido.
    /// </remarks>
    /// </summary>
    public class TileAnimationComponent: SoftComponent
    {
        private readonly int DEFAULT_WIDTH = CharSet.PATTERN.PATTERN_RPG_2K.CHAR_WIDTH;
        private readonly int DEFAULT_HEIGHT = CharSet.PATTERN.PATTERN_RPG_2K.CHAR_HEIGHT;
        #region fields
        private TileSet mTileSet = null;

        private GameObject mParent = null;

        private int mActualTile = TileSet.INVALID_TILESET;

        private bool mEnabled = true;

        private Point mPointCenter;
        #endregion

        #region properties
        public Point pointCenter
        {
            get{return mPointCenter;}
            set{mPointCenter = value;}
        }

        public int actualTile
        {
            get{return mActualTile;}
            set { mActualTile = value; }
        }

        #endregion
        public TileAnimationComponent(string name):this(name, TileSet.INVALID_TILESET)
        {
        }

        public TileAnimationComponent(string name, int initTile)
        {
            mPointCenter = new Point(12, DEFAULT_HEIGHT);
            mActualTile = initTile;
            mTileSet = new TileSet(name, DEFAULT_WIDTH, DEFAULT_HEIGHT);
        }

        #region SoftComponent Members

        public void draw(Microsoft.Xna.Framework.GameTime time, ref Microsoft.Xna.Framework.Rectangle rectWindow)
        {
            if (actualTile == TileSet.INVALID_TILESET) return;

            Rectangle rectSource = mTileSet.getRect(mActualTile);
            Vector2 source = new Vector2(mParent.position.X - pointCenter.X - rectWindow.X, mParent.position. Y - pointCenter.Y - rectWindow.Y); 
            RectFloat rectDest = new RectFloat(source, new Vector2(mTileSet.getTileWidth(), mTileSet.getTileHeight()));
            SpriteBatch sb = RPG.me.spriteBatch;
            sb.Draw(mTileSet.getTexture(), rectDest.toRectangle(), rectSource, Color.White);  
        }

        public void update(Microsoft.Xna.Framework.GameTime time)
        {
        }

        public bool init(GameObject parent)
        {
            mParent = parent;
            return true;
        }

        public void ejected()
        {
           
        }

        public bool ejectMe()
        {
            return false;
        }

        public bool enable
        {
            get { return mEnabled; }
            set { mEnabled = value; }
        }

        #endregion
    }
}
