using System;
using System.Collections.Generic;
using System.Text;
using MotivationalSystem;
using Microsoft.Xna.Framework;
using System.Reflection;
using MotivationalSystem.DAO;
using System.Xml;
namespace edde.components.motsystem
{
    public class MotSystemComponent:SoftComponent, IMotAgent
    {
        /// <summary>
        /// Tempo de espera para um passo de execu��o do sistema motivacional.
        /// </summary>
        private static TimeSpan DELAY_TIME = new TimeSpan(0, 0, 0, 0, 20);

        #region members

        /// <summary>
        /// Refer�ncia a um DAO gerador de Agentes.
        /// </summary>
        private static IMotAgentDAO ms_MotAgentDAO = null; 

        /// <summary>
        /// Nome da classe de MotSystem carregada
        /// </summary>
        private string mStrMotSystemClass = "";

        /// <summary>
        /// Refer�ncia ao Agente Motivational.
        /// </summary>
        private MotivationalSystem.IMotAgent mMotAgent;

        /// <summary>
        /// Tempo em que foi executado o �ltimo passo do sistema motivacional.
        /// </summary>
        private TimeSpan mPrevTime = TimeSpan.Zero;

        /// <summary>
        /// Se o componente est� habilitado ou n�o.
        /// </summary>
        private bool mEnabled = true;

        #endregion

        #region properties
        public string motSystemClass
        {
            get { return mStrMotSystemClass; }
        }
        #endregion

        /// <summary>
        /// Construtor
        /// </summary>
        public MotSystemComponent(string strClass)
        {
            //Verifica se o DAO dos agentes n�o est� constru�do
            if (ReferenceEquals(ms_MotAgentDAO, null))
            {
                //inicia f�brica de controlles
                XmlReader xml = Resource.getXML("motsystem");
                ms_MotAgentDAO = MotivationalSystem.Factory.getXMLDAO(xml);
            }


            //Obt�m uma c�pia do Agente.
           mMotAgent = ms_MotAgentDAO.getMotAgent(strClass);
           if (mMotAgent == null) throw new edde.components.SoftComponentException("Imposs�vel achar sistema motivacional classe " + strClass, this, edde.eddeException.REGION_LOOP.LOAD);

           mMotAgent.randomizeProcessorsValues(10);
           mStrMotSystemClass = strClass;
        }

        /// <summary>
        /// Retorna um container contendo os processos.
        /// </summary>
        public ICollection<IProcessor> ContentProcessor
        {
            get { return mMotAgent.ContentProcessor; }
        }

        public bool isUpdateTime(TimeSpan actualTime)
        {
            return (actualTime - mPrevTime) > DELAY_TIME;
        }

        #region SoftComponent Members

        /// <summary>
        /// N�o deve fazer nada pq este componente � apenas de IA.
        /// </summary>
        /// <param name="time"></param>
        /// <param name="rectWindow"></param>
        public void draw(Microsoft.Xna.Framework.GameTime time, ref Rectangle rectWindow)
        {
            //nada
        }

        /// <summary>
        /// Chamada da engine de Game (EDDE) para atualizar este componente. Este m�todo controla o tempo e delega a atualiza��o pro IMotAgent.
        /// </summary>
        /// <param name="time">Tempo atual do sistema.</param>
        public void update(Microsoft.Xna.Framework.GameTime time)
        {
            TimeSpan actualTime = time.TotalGameTime;

            TimeSpan delayTime = actualTime - mPrevTime;

            update(actualTime, delayTime);        
        }

        /// <summary>
        /// Inicia o componente
        /// </summary>
        /// <param name="_parent">GameObject que cont�m este componente</param>
        /// <returns></returns>
        public virtual bool init(GameObject _parent)
        {         
            return true;
        }

        /// <summary>
        /// m�todo chamado quando o componente � ejetado
        /// </summary>
        public void ejected()
        {
        }

        /// <summary>
        /// Diz ao GameObject se deve remov�-lo.
        /// </summary>
        /// <returns></returns>
        public bool ejectMe()
        {
            return false;
        }

        /// <summary>
        /// Verifica se o componente est� habilitado ou n�o.
        /// </summary>
        public bool enable
        {
            get { return mEnabled; }
        }

        #endregion



        #region IMotAgent Members


        public void setAction(string id, IAction action)
        {
            mMotAgent.setAction(id, action);
        }

        public void update(TimeSpan actualTime, TimeSpan deltaTime)
        {
            if (deltaTime > DELAY_TIME)
            {
                mMotAgent.update(actualTime, deltaTime);
                mPrevTime = actualTime;
            }
        }         


        public ISensor getSensor(string id)
        {
            return mMotAgent.getSensor(id);
        }


        public float[] getEmotionalColor()
        {
            return mMotAgent.getEmotionalColor();
        }

      
        public float[] getBlendEmotionalColor()
        {
            return mMotAgent.getBlendEmotionalColor();
        }


        public void randomizeProcessorsValues(float range)
        {
            mMotAgent.randomizeProcessorsValues(range);
        }

        public string  getVisibleEmotion()
        {
            return mMotAgent.getVisibleEmotion();
        }

        public float wellBeing()
        {
            return mMotAgent.wellBeing();
        }


        public float badBeing()
        {
            return mMotAgent.badBeing();
        }

        #endregion
    }
}
