﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using MotivationalSystem;

namespace edde.components.motsystem
{
    /// <summary>
    /// Classe que gera log dos processos
    /// </summary>
    public class LogMotSystemComponent:  SoftComponent
    {
        /// <summary>
        /// Referência ao motSystem
        /// </summary>
        [Injection()]
        MotSystemComponent mMotSystemComponent = null;

        ICollection<IProcessor> mCollProcessor;

        List<float> mListMediaProcessors; //lista do valor médio dos processos

        TimeSpan mLastTime = TimeSpan.Zero;
        TimeSpan mLastUpdateTime = TimeSpan.Zero;

        readonly TimeSpan DELAY_UPDATE = TimeSpan.FromSeconds(1);

        private GameObject mParent = null;

        /// <summary>
        /// referencia ao GameObject dono
        /// </summary>
        public GameObject parent { get { return mParent; } }


        private string mNameFile;

        public string nameFile { get { return mNameFile; } }

        public LogMotSystemComponent(string nameFile)
        {
            mNameFile = nameFile;
        }
        ///<summary>
        ///Um sstream para gravação do log
        StreamWriter mStreamWrite;

        #region SoftComponent Members

        public void draw(Microsoft.Xna.Framework.GameTime time, ref Microsoft.Xna.Framework.Rectangle rectWindow)
        {
            //
        }

        public void update(Microsoft.Xna.Framework.GameTime time)
        {
            mLastTime = time.TotalGameTime;

            if (mLastTime - mLastUpdateTime > DELAY_UPDATE)
            {
                mLastUpdateTime = mLastTime;
                writeData();

                int i = 0;
                foreach (IProcessor p in mCollProcessor)
                {
                    mListMediaProcessors[i] = p.getMediaValues();
                    i++;
                }
            }
            else
            {
                int i = 0;
                foreach(IProcessor p in mCollProcessor)
                {
                    mListMediaProcessors[i] = (mListMediaProcessors[i] + p.getMediaValues()) / 2f;
                    i++;
                }
            }
        }

        public bool init(GameObject _parent)
        {
            mParent = _parent;
            mCollProcessor = mMotSystemComponent.ContentProcessor;
            mListMediaProcessors = new List<float>(mCollProcessor.Count);

            if(ReferenceEquals(parent, null)) return false;
            try
            {
                mStreamWrite = new StreamWriter("C:\\detetive\\" +mNameFile+ ".log");
                writeHead();
            }
            catch (Exception)
            {
                throw new edde.components.SoftComponentException("Não foi possível iniciar o stream de dados do sistema de log", this, eddeException.REGION_LOOP.LOAD);
            }

            for (int i = 0; i < mCollProcessor.Count; i++)
            {
                mListMediaProcessors.Add(0f);
            }
            return true;
        }

        public void ejected()
        {
            mStreamWrite.Close();
        }

        public bool ejectMe()
        {
            return false;
        }

        public bool enable
        {
            get { return true; }
        }

        #region write methods
        public void writeHead()
        {
            mStreamWrite.Write("Time\t");

            foreach (IProcessor p in mCollProcessor)
            {
                mStreamWrite.Write(p.name + "\t");
            }

            mStreamWrite.Write("Emocao Corrente\t");

            mStreamWrite.WriteLine("");
        }
        public void writeData()
        {
            mStreamWrite.Write(string.Format("{0:G}\t", mLastTime.TotalSeconds));
            int i = 0;
            foreach (IProcessor p in mCollProcessor)
            {
                mStreamWrite.Write(string.Format("{0:G}\t", mListMediaProcessors[i]));
                i++;
            }

            mStreamWrite.Write(mMotSystemComponent.getVisibleEmotion() + "\t");
            mStreamWrite.WriteLine("");
        }
        #endregion

        #endregion
    }
}
