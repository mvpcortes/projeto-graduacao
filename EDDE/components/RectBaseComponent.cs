﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace edde.components
{
    /// <summary>
    /// Força desenhar o rectBase como um quadrado
    /// </summary>
    public class RectBaseComponent:SoftComponent
    {
        GameObject mParent = null;

        PrimitiveBatch mPrimitiveBatch = null;

        public GameObject parent
        {
            get { return mParent; }
        }
        #region SoftComponent Members

        public void draw(Microsoft.Xna.Framework.GameTime time, ref Microsoft.Xna.Framework.Rectangle rectWindow)
        {
            RectFloat rect = new RectFloat(parent.position - parent.getSizeBase() / 2f, parent.getSizeBase());

            rect.left -= rectWindow.X;
            rect.top -= rectWindow.Y;
            RPG.me.spriteBatch.End();
                mPrimitiveBatch.Begin(Microsoft.Xna.Framework.Graphics.PrimitiveType.LineList);
                mPrimitiveBatch.AddVertex(new Vector2(rect.left, rect.top), Color.White);
                mPrimitiveBatch.AddVertex(new Vector2(rect.right, rect.top), Color.White);

                mPrimitiveBatch.AddVertex(new Vector2(rect.right, rect.top), Color.White);
                mPrimitiveBatch.AddVertex(new Vector2(rect.right, rect.bottom), Color.White);

                mPrimitiveBatch.AddVertex(new Vector2(rect.right, rect.bottom), Color.White);
                mPrimitiveBatch.AddVertex(new Vector2(rect.left , rect.bottom), Color.White);

                mPrimitiveBatch.AddVertex(new Vector2(rect.left, rect.bottom), Color.White);
                mPrimitiveBatch.AddVertex(new Vector2(rect.left, rect.top), Color.White);
                mPrimitiveBatch.End();
            RPG.me.spriteBatch.Begin();

        }

        public void update(Microsoft.Xna.Framework.GameTime time)
        {
           
        }

        public bool init(GameObject _parent)
        {
            mParent = _parent;
            mPrimitiveBatch = new PrimitiveBatch(RPG.me.GraphicsDevice);
            return mParent!= null;
        }

        public void ejected()
        {
            
        }

        public bool ejectMe()
        {
            return false;
        }

        public bool enable
        {
            get { return true; }
        }

        #endregion
    }
}
