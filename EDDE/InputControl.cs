using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;

namespace edde
{
    /// <summary>
    /// Este componente monitora os controles de entrada (teclado/joystick) para um controle abstrato
    /// </summary>
    public class InputControl : Microsoft.Xna.Framework.GameComponent
    {

        /// <summary>
        /// Evento de desativa��o do focus do windows
        /// </summary>
        /// <param name="sender">Objeto que enviou o evento</param>
        /// <param name="e">parametros do evento</param>
        private void onDeactivated(object sender, EventArgs e)
        {
            mbRefreshKeys = true;
        }


        /// <summary>
        /// Boleano que indica que se deve tornar as teclas nulas.
        /// </summary>
        private bool mbRefreshKeys;

        /// <summary>
        /// Enumera��o especificando as teclas l�gicas do EDDE.
        /// </summary>
        public enum RPG_KEY
        { 
            NONE = -1,
	        UP   =  0,
            DOWN =  1,
            LEFT =  2,        	 
	        RIGHT=  3,
            ACTION_A=4,
            ACTION_B=5,
            ACTION_C=6,
            ACTION_D=7,
            ACTION_L=8,
            ACTION_R=9,
            START=10,
            LENGTH=11
        }

        private bool[] mKeyCurrent;

        private bool[] mKeyPrev;

        private TimeSpan[] mTimeLastKey;
        
        //private TimeSpan mTimeLastKeyPrev[KEY.LENGTH];

        private Dictionary<Microsoft.Xna.Framework.Input.Keys, RPG_KEY> mMapKeyboard;

        public InputControl(Game game)
            : base(game)
        {
            Game.Deactivated += onDeactivated;

            mbRefreshKeys  = false;
            mKeyCurrent = new bool[(int)RPG_KEY.LENGTH];
            mKeyPrev    = new bool[(int)RPG_KEY.LENGTH];
            mTimeLastKey= new TimeSpan[(int)RPG_KEY.LENGTH];
          //  mTimeLastKeyPrev= new TimeSpan[RPG_KEY.LENGTH];
            mMapKeyboard = new Dictionary<Keys, RPG_KEY>();
        }

        public void initDefaultKeys()
        {
            mapKeyboard(Keys.Up         , RPG_KEY.UP);
            mapKeyboard(Keys.Down       , RPG_KEY.DOWN);
            mapKeyboard(Keys.Left       , RPG_KEY.LEFT);
            mapKeyboard(Keys.Right      , RPG_KEY.RIGHT);
            mapKeyboard(Keys.A          , RPG_KEY.ACTION_A);
            mapKeyboard(Keys.Z          , RPG_KEY.ACTION_B);
            mapKeyboard(Keys.S          , RPG_KEY.ACTION_C);
            mapKeyboard(Keys.X          , RPG_KEY.ACTION_D);
            mapKeyboard(Keys.D          , RPG_KEY.ACTION_L);
            mapKeyboard(Keys.C          , RPG_KEY.ACTION_R);
        }
        /// <summary>
        /// iniciando sistemas
        /// </summary>
        public override void Initialize()
        {
            TimeSpan tempTime = new TimeSpan();
            for(int i = 0; i < (int)RPG_KEY.LENGTH; i++)
            {
               mKeyCurrent[i] = mKeyPrev[i] = false;
               mTimeLastKey[i] = /*mTimeLastKeyPrev[i]*/ tempTime;
            }
            initDefaultKeys();
            base.Initialize();
        }

        /// <summary>
        /// Atualiza o sistema de teclas.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            Microsoft.Xna.Framework.Input.KeyboardState keyState = Keyboard.GetState();

            for(int i = 0; i < (int)RPG_KEY.LENGTH; i++)
            {
                mKeyPrev[i] = mKeyCurrent[i];
            }

            foreach(KeyValuePair<Keys, RPG_KEY> keyPair in mMapKeyboard)
            {
                int value = (int)keyPair.Value;
                if (!mbRefreshKeys)
                    mKeyCurrent[value] = keyState.IsKeyDown(keyPair.Key);
                else
                    mKeyCurrent[value] = false;

                if(mKeyCurrent[value] && !mKeyPrev[value])
                {   
                    //mTimeLastKeyPrev[keyPair.Value] = mTimeLastKey[keyPair.Value];
                    mTimeLastKey[value] = gameTime.TotalGameTime;
                }
            }

            mbRefreshKeys = false;
        }
        
        /// <summary>
        /// Mapeia uma tecla de teclado para uma tecla l�gica de rpg
        /// </summary>
        /// <param name="key">A tecla de teclado</param>
        /// <param name="rpgkey"> A tecla do rpg</param>
        public void mapKeyboard(Keys key, RPG_KEY rpgkey)
        {
            mMapKeyboard[key] = rpgkey;
        }

        /// <summary>
        /// Remove mapeamento de uma tecla de teclado para uma tecla l�gica
        /// </summary>
        /// <param name="key">A tecla de teclado</param>
        /// <param name="rpgkey"> A tecla do rpg</param>
        public RPG_KEY unMapKeyboard(Keys key)
        {
            RPG_KEY rk = RPG_KEY.NONE;
            mMapKeyboard.TryGetValue(key, out rk);
            mMapKeyboard.Remove(key);
            return rk;
        }

        /// <summary>
        /// Remove mapeamento feio a uma  RPG_KEY.
        /// </summary>
        /// <param name="rpg_key"> A tecla do rpg</param>
        public void unMapAllRPG_KEY(RPG_KEY rpg_key)
        {
            LinkedList<Keys> listTemp = new LinkedList<Keys>();
            foreach(KeyValuePair<Keys, RPG_KEY> keyPair in mMapKeyboard)
            {
                if(keyPair.Value == rpg_key)
                    listTemp.AddLast(keyPair.Key);
            }

            foreach(Keys key in listTemp)
                mMapKeyboard.Remove(key);
        }

        /// <summary>
        /// Verifica se a tecla foi pressionada.
        /// </summary>
        /// <param name="key">a tecla a ser consultada</param>
        /// <returns>se a tecla foi pressionada ou n�o</returns>    	 
	    public bool isKeyPress(RPG_KEY key) 
        {
            return mKeyCurrent[(int)key];
        }

        /// <summary>
        /// Verifica se a tecla come�ou a ser precionada (down) este instante.
        /// </summary>
        /// <param name="key">a tecla a ser consultada</param>
        /// <returns>se a tecla foi <i>down</i> ou n�o</returns>    	 
        public bool isKeyDown(RPG_KEY key) {
            return mKeyCurrent[(int)key] && !mKeyPrev[(int)key];
        }
    	 
        /// <summary>
        /// Verifica se a tecla foi largada.
        /// </summary>
        /// <param name="key">a tecla a ser consultada</param>
        /// <returns>se a tecla foi largada  ou n�o</returns>
        public bool isKeyUp(RPG_KEY key) {
            return !mKeyCurrent[(int)key] && mKeyPrev[(int)key];
        }
    	 
        /// <summary>
        /// Ainda n�o implementado, especifica que uma tecla recebeu duplo clique
        /// </summary>
        /// <param name="key">a tecla a ser consultada</param>
        /// <returns>se a tecla recebeu duplo clique ou n�o</returns>
        public bool isDoublePress(RPG_KEY key) {
            return false;
        }
    }
}