﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using edde.components;
using System.Reflection;

namespace edde.resourcing
{
    /// <summary>
    /// Interface que define um objecto que pode concorrer por componentes dentro de um GameObjectResourced;
    /// </summary>
    public interface ISoftComponentOwner
    {
        int resourcePriority
        {
            get;
        }

        GameObjectResourced parentResourced
        {
            get;
        }

        void onGiveBackResources();

        void addResources(ICollection<SoftComponent> collResource);

        void addResource(SoftComponent resource);
    }

    /// <summary>
    /// Tag de atributo que define que o componente além de injetado é um componente que é concorrido como recurso por outros componentes (ISoftComponentOwner)
    /// </summary>
    public class InjResource:System.Attribute {}

    /// <summary>
    /// Classe que implementa um GameObject que pode prover SoftComponents como se fossem recursos a SoftComponents do Tipo SoftComponentOwner
    /// </summary>
    public class GameObjectResourced : GameObject
    {
        /// <summary>
        /// Timer usado para interromper um dono de recurso
        /// </summary>
        [Injection]
        private Timer<GameObjectResourced> mTimer;

        /// <summary>
        /// Dicionário contendo um mapeamento de recurso para seus donos
        /// </summary>
        Dictionary<SoftComponent, ISoftComponentOwner> mDicComponentOwner = new Dictionary<SoftComponent, ISoftComponentOwner>();

        public GameObjectResourced()
        {
            mTimer = new Timer<GameObjectResourced>();
        }

        #region GameObject Methods
        public override bool init(Scene scene)
        {
            if (base.init(scene))
            {
                initResources();
                return true;
            }

            return false;
        }
        #endregion


        #region resource manager methods
        public bool tryGetComponents(ISoftComponentOwner scOwner, TimeSpan delay, params SoftComponent[] vecSC)
        {

            foreach (SoftComponent sc in vecSC)
            {
                ISoftComponentOwner otherScOwner = null;
                if (mDicComponentOwner.TryGetValue(sc, out otherScOwner))
                {
                    if (otherScOwner.resourcePriority >= scOwner.resourcePriority)
                        return false;

                    otherScOwner.onGiveBackResources();
                    //mTimer.removeTimeWithObject(otherScOwner);
                }

                mDicComponentOwner[sc] = scOwner;
            }

            mTimer.addTimer(delay, TimeSpan.Zero, scOwner, onGiveBackResources);
            return true;
        }

        /// <summary>
        /// Evento de tempo que ocorre quando a janela de tempo de um dono de recurso acaba
        /// </summary>
        private void onGiveBackResources(Timer<GameObjectResourced>.TimeParam timeParam, GameObjectResourced gor)
        {
            ISoftComponentOwner scOwner = timeParam.objParam as ISoftComponentOwner;
            if (scOwner != null)
            {
                scOwner.onGiveBackResources();

                LinkedList<SoftComponent> listRemove = new LinkedList<SoftComponent>();
                foreach (KeyValuePair<SoftComponent, ISoftComponentOwner> key in mDicComponentOwner)
                {
                    if (ReferenceEquals(key.Value, scOwner))
                        listRemove.AddLast(key.Key);
                }

                foreach (SoftComponent sc in listRemove)
                {
                    mDicComponentOwner.Remove(sc);
                }
            }
        }

        public bool IsOwner(SoftComponent sc, ISoftComponentOwner owner)
        {
            ISoftComponentOwner realOwner;
            if (mDicComponentOwner.TryGetValue(sc, out realOwner))
            {
                return ReferenceEquals(owner, realOwner);
            }
            return false;
        }

        //Vetor contendo uma lista de SoftComponents a serem inseridos em um SoftComponentOwner
        private static List<SoftComponent> ms_listComponentInsertOwner = new List<SoftComponent>();
        private void initResources()
        {
            ms_listComponentInsertOwner.Clear();
            foreach (SoftComponent s in components)
            {
                ISoftComponentOwner owner = s as ISoftComponentOwner;
                if (owner != null)
                {
                    Type type = owner.GetType();
                    FieldInfo[] vecMemberInfo = type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                    foreach (FieldInfo mi in vecMemberInfo)
                    {
                        object[] vecObjInject = mi.GetCustomAttributes(true);
                        bool injection = false;
                        bool resource = false;
                        foreach (object o in vecObjInject)
                        {
                            injection |= o is edde.Injection;
                            resource |= o is InjResource;
                        }

                        if (injection && resource)
                        {
                            SoftComponent insert = mi.GetValue(owner) as SoftComponent;
                            if (insert != null) owner.addResource(insert);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Devolve Componentes para o GameObject
        /// </summary>
        /// <param name="scOwner"></param>
        /// <param name="vecSC"></param>
        public void freeComponents(ISoftComponentOwner scOwner, params SoftComponent[] vecSC)
        {
            freeComponents(scOwner, (IEnumerator<SoftComponent>)vecSC.GetEnumerator());
        }

        public void freeComponents(ISoftComponentOwner scOwner, ICollection<SoftComponent> coll)
        {
            freeComponents(scOwner, coll.GetEnumerator());
        }
        public void freeComponents(ISoftComponentOwner scOwner, IEnumerator<SoftComponent> enumerator)
        {
            while(enumerator.MoveNext())
            {
                SoftComponent sc = enumerator.Current;
                ISoftComponentOwner owner;
                if (mDicComponentOwner.TryGetValue(sc, out owner))
                {
                    if (ReferenceEquals(owner, scOwner))
                        mDicComponentOwner.Remove(sc);
                    else
                        throw new SoftComponentException("Tentativa de devolver componente de dono errado.", sc);
                }
            }
        }
        #endregion
    }
}
