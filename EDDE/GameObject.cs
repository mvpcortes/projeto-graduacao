using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using System.Reflection;
using edde;
using edde.physicalmanager;

namespace edde
{
    /// <summary>
    /// Classe que determina um objeto de jogo. Ela � baseada na SimpleSoftComponentContainer, mas deveria ser baseada na SoftComponentContainer e implementar os detalhes manipula��o do container
    /// </summary>
    public class GameObject : SimpleSoftComponentContainer, ICollised
    {
        #region fields

        /// <summary>
        /// Regi�o padr�o para �rea de colis�o
        /// </summary>
        public static readonly RectFloat DEFAULT_ICOLLISED_REGION = new RectFloat(Vector2.One * (-8f), Vector2.One * (16f));

        /// <summary>
        /// Vetor de posi��o central do GameObject
        /// </summary>
    	private Vector2 mPos;

        /// <summary>
        /// Backup da posi��o pr�via a um movimento. Usado pelo gerenciador de f�sica para reverter um movimento.
        /// </summary>
        private Vector2 mPosPrev;

        /// <summary>
        /// Vetor de dire��o do GameObject
        /// </summary>
        private Vector2 mDir;
 
        /// <summary>
        /// Ret�ngulo que indica a "base". Isto �, a regi�o do GameObject que est� "colada" no ch�o de um Scene ou de um Map. Ele sempre envolve a posi��o central do GameObject
        /// </summary>
        private RectFloat mRectBase;
    	 
        /// <summary>
        /// Determina se o GameObject � um s�lido
        /// </summary>
        private COLLISION_TYPE mCollisedType;

        /// <summary>
        /// verifica se est� movendo ou n�o
        /// </summary>
        private bool mIsMoving;

        #endregion

        /// <summary>
        /// Construtor
        /// </summary> 
        public GameObject()
        {
            mCollisedType = COLLISION_TYPE.SOLID;

            mRectBase = DEFAULT_ICOLLISED_REGION;
            mDir = MyMath.VEC_UP;
            mIsMoving = false;
            mPos = mPosPrev = new Vector2(36, 36);
        }

       

        /// <summary>
        /// Faz busca em componentes injetados dentro de outros componentes
        /// </summary>
        /// <param name="softComponent">O componente alvo onde ser� procurado seus componentes injetados</param>
        /// <param name="parent">O GameObject que cont�m o softComponent</param>
        private static void findInjectedComponent(SoftComponent softComponent, GameObject parent)
        {
            findInjectedComponent(softComponent, parent, softComponent.GetType());
        }

        /// <summary>
        /// Faz busca em componentes injetados dentro de outros componentes
        /// </summary>
        /// <param name="softComponent">O componente alvo onde ser� procurado seus componentes injetados</param>
        /// <param name="type">O tipo a ser considerado durante a busca por inje��es</param> 
        private static void findInjectedComponent(SoftComponent softComponent, GameObject parent, Type type)
        {
            if (type == typeof(object) || type.GetInterface(typeof(SoftComponent).Name) == null)//para de buscar inje��o pois chegou em uma classe base que n�o possui a interface SoftComponent
                return;

            FieldInfo[] vecMemberInfo = type.GetFields(BindingFlags.GetField | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (FieldInfo mi in vecMemberInfo)
            {
                Injection[] vecObjInject = (Injection[])mi.GetCustomAttributes(typeof(edde.Injection), true);
                if (vecObjInject.Length > 0)
                {
                    SoftComponent sc = null;
                    if (vecObjInject[0].diferenctial == null || vecObjInject[0].diferenctial == "")
                        sc = parent.getComponent(mi.FieldType);
                    else
                    {
                        sc = parent.getComponent(mi.FieldType, vecObjInject[0].diferenctial);

                    }

                    if (!ReferenceEquals(sc, null))
                    {
                        mi.SetValue(softComponent, sc);
                    }
                    else
                    {
                        throw new InjectionException(parent, "N�o foi poss�vel injetar um componente dentro de outro");
                    }
                }
            }
            findInjectedComponent(softComponent, parent, type.BaseType);
        }

        /// <summary>
        /// Busca Componentes injetados dentro do GameObject
        /// </summary>
        protected void findInjectedItems()
        {
            findInjectedItems(this, this);
        }

        /// <summary>
        /// Busca Componentes injetados dentro do GameObject
        /// </summary>
        /// <param name="dest">O Game object que receber� as inje��es</param>
        /// <param name="source">O Game objeto fonte dos dados a serem injetados</param>
        internal static void findInjectedItems(GameObject dest, object source)
        {
            findInjectedItems(dest, source, source.GetType());
        }
        /// <summary>
        /// Busca Componentes injetados dentro do GameObject
        /// </summary>
        /// <param name="dest">O Game object que receber� as inje��es</param>
        /// <param name="source">O Game objeto fonte dos dados a serem injetados</param>
        /// <param name="type">O tipo a ser usado na busca dos itens injetados</param>
        private static void findInjectedItems(GameObject dest, object source, Type type)
        {
            if (type == typeof(GameObject) || type == typeof(object))
                return;

            FieldInfo[] vecMemberInfo = type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            foreach (FieldInfo mi in vecMemberInfo)
            {
                object[] vecObjInject = mi.GetCustomAttributes(typeof(edde.Injection), true);
                if (vecObjInject.Length > 0)
                {
                    Object obj = mi.GetValue(source);

                    if (ReferenceEquals(obj, null)) throw new InjectionException(dest, "Item injetado n�o constru�do.");

                    SoftComponent component = obj as SoftComponent;
                    if (!ReferenceEquals(component, null))
                        dest.addComponent(component, mi.Name);
                    else
                        throw new InjectionException(dest, "Item injetado n�o � um SoftComponent.");
                }
            }

            findInjectedItems(dest, source, type.BaseType);
        }

        public void addAndInitComponent(SoftComponent component)
        {
            findInjectedComponent(component, this);
            if (!component.init(this))
            {
                throw new edde.components.SoftComponentException("N�o foi poss�vel iniciar o componente", component);
            }
            addComponent(component);

        }


        /// <summary>
        /// Implementa impress�o do GameObject. Imprimindo seus componentes.
        /// </summary>
        /// <param name="time">O tempo do jogo</param>
        public virtual void draw(GameTime time, ref Rectangle rectWindow)
        {
            foreach(SoftComponent comp in components)
            {
                if(comp.enable)
                    comp.draw(time, ref rectWindow);
            }
        }
    	 
        /// <summary>
        /// Implementa o update do GameObject. Varrendo seus componentes.
        /// </summary>
        /// <param name="time">O tempo do jogo</param>
        public virtual void update(GameTime time)
        {
            List<SoftComponent> listRemove = new List<SoftComponent>();
            foreach (SoftComponent comp in components)
            {
                if(comp.enable)
                    comp.update(time);

                if (comp.ejectMe())
                    listRemove.Add(comp);
            }

            foreach (SoftComponent comp in listRemove)
            {
                removeComponent(comp);
            }
        }
    	 
        /// <summary>
        /// Inicia o GameObject em determinado Scene
        /// </summary>
        /// <param name="scene">O scen�rio onde o GameObject est� inserido</param>
        /// <returns>Se a opera��o foi realizada com sucesso</returns>
        public virtual bool init(Scene scene)
        {
            //injection
            findInjectedItems();
            foreach (SoftComponent softComp in components)
            {
                findInjectedComponent(softComp, this);
                if (!softComp.init(this))
                {
                    throw new edde.components.SoftComponentException("N�o foi poss�vel iniciar o componente", softComp);
                }
            }

            backupPosition();
            return true;
        }

        /// <summary>
        /// Finaliza um GameObject quando a aplica��o � encerrada ou o cen�rio � removido definitivamente
        /// </summary>
        /// <param name="scene"></param>
        public void finish(Scene scene)
        {
            foreach (SoftComponent sc in components)
            {
                sc.ejected();
            }
        }

        /// <summary>
        /// Atribui uma dire��o ao GameObject
        /// </summary>
        /// <param name="dir">A nova dire��o do GameObject</param>
        public void setDirection(Vector2 dir)
        {
            if (dir != Vector2.Zero)
            {
                mDir = dir;
                mDir.Normalize();
            }

            if (float.IsNaN(mDir.X) || float.IsNaN(mDir.Y))
                throw new edde.eddeException("N�mero inv�lido");
        }

        public Vector2 getDirection() { return mDir; }

        /// <summary>
        /// Retorna a posi��o do GameObject no mapa
        /// </summary>
        /// <returns></returns>
        public Vector2 position
        {
            get {
                return mPos;
            }
            set {
                mPos = value;
            }
        }
      

        public void setPosition(Vector2 pos)
        {
            mPos = pos;
        }

        public void incPosition(float x, float y)
        {
            Vector2 tempPos = mPos;
            mPos.X += x;
            mPos.Y += y;
            if (float.IsInfinity(mPos.X) || (float.IsNaN(mPos.X)))
                throw new eddeException("Erro de soma de vetor");
        }

        public void incPosition(Vector2 incVec)
        {
            Vector2 tempPos = mPos;
            mPos += incVec;
            if (float.IsInfinity(mPos.X) || (float.IsNaN(mPos.X)))
                throw new eddeException("Erro de soma de vetor");
        }



        #region ICollised Members

        /// <summary>
        /// Determina se um rect interceptou este objeto
        /// </summary>
        /// <param name="rect">O rect a ser testado</param>
        /// <returns>verdadeiro se interceptou</returns>
        public bool intersects(RectFloat rect)
        {
            return Collision.intersects(getRectBase(), rect);
        }

        /// <summary>
        /// Retorna o ret�ngulo base do GameObject, j� deslocado considerando a posi��o do GameObject
        /// </summary>
        /// <returns></returns>
        public RectFloat getRectBase()
        {
            RectFloat r = new RectFloat();
            r.setSource(
                mPos.X + (mRectBase.getSource().X),
                mPos.Y + (mRectBase.getSource().Y)
            );
            r.setSize(
                (mRectBase.getSize().X),
                (mRectBase.getSize().Y)
            );
            return r;
        }

        /// <summary>
        /// Retorna o tamanho da �rea de colis�o
        /// </summary>
        /// <returns></returns>
        public Vector2 getSizeBase()
        {
            return mRectBase.getSize();
        }

        public edde.physicalmanager.COLLISION_TYPE getTypeCollision()
        {
            return mCollisedType;
        }

        #endregion

        public void setTypeCollision(COLLISION_TYPE type) { mCollisedType = type; }

        public void setRectBase(RectFloat rect)
        {
            mRectBase = rect;
        }

        

        public bool moving
        {
            get { return mIsMoving; }
            set { mIsMoving = value; }
        }


        public virtual bool onColision(System.Collections.Generic.ICollection<ICollised> collObject)
        {
            if (collObject.Count > 0) //se colidiu com um wall, sempre volta
                return true;

            return false;
        }

        public virtual void onTouch(System.Collections.Generic.ICollection<ICollised> collObject)
        {
            return;
        }

        internal void restorePrevPosition()
        {
            mPos = mPosPrev;
        }

        internal void backupPosition()
        {
            mPosPrev = mPos;
        }

        public virtual bool testVision(GameTime gameTime)
        {
            return false;
        }

        public virtual void verifyVisionContainer(ICollection<ICollised> collVision)
        {
            //faznada
        }
    }
}
