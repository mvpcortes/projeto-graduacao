using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace edde
{
    public interface SoftComponentContainer
    {
        /// <summary>
        /// Adiciona um componente ao container
        /// </summary>
        /// <param name="comp">o componente a ser inserido</param>
        /// <returns></returns>
	    bool addComponent(SoftComponent comp);

        /// <summary>
        /// Remove um componente espec�fico ao container.
        /// </summary>
        /// <param name="comp">O componente a ser removido.</param>
        /// <returns>true se conseguiu remover o componente</returns>
	    bool removeComponent(SoftComponent comp);

        /// <summary>
        /// remove todas os componentes de determinada classe (tipo)
        /// </summary>
        /// <param name="className">a classe do componente a ser removido</param>
        /// <returns>o n�mero de componentes removidos</returns>
        int removeComponent(Type className);

        /// <summary>
        /// Retorna o primeiro componente da class especificada por className
        /// </summary>
        /// <param name="className">A classe do componente a ser removido</param>
        /// <returns> O componente removido. null se n�o achar nenhum desta classe</returns>
        SoftComponent getComponent(Type className);

        /// <summary>
        /// Retorna todos os componentes da classe className.
        /// </summary>
        /// <param name="className">A classe do componente a buscada</param>
        /// <param name="container">Um container que receber� os itens removidos</param>
        /// <returns>o n�mero de componentes removidos</returns>
	    int getComponent(Type className, System.Collections.Generic.ICollection<SoftComponent> container);

        /// <summary>
        /// Retorna se determinado componente existe no container
        /// </summary>
        /// <param name="component">O componente a ser buscado</param>
        /// <returns>se existe sim (true) ou n�o (false)</returns>
	    bool referenceComponent(SoftComponent component);

        //for�a a inicializa��o dos componentes
        //void initComponents();
    }

    public abstract class SimpleSoftComponentContainer: SoftComponentContainer
    {
        private List<SoftComponent> mListComponents;

        //atalho para softComponents que possuem nome adicionado pelo GameObject
        private List<KeyValuePair<string, SoftComponent>> mListCompShortCut;

        public SimpleSoftComponentContainer()
        {
            mListCompShortCut = new List<KeyValuePair<string, SoftComponent>>();
            mListComponents = new List<SoftComponent>();
        }

        #region SoftComponentContainer Members

        public bool addComponent(SoftComponent comp)
        {
            mListComponents.Add(comp);
            return true;
        }

        internal void addComponent(SoftComponent comp, string shortcut)
        {
            addComponent(comp);
            mListCompShortCut.Add(new KeyValuePair<string, SoftComponent>(shortcut, comp));
        }

        public bool removeComponent(SoftComponent comp)
        {
            mListComponents.Remove(comp);
            
            List<KeyValuePair<string, SoftComponent>> newList = new List<KeyValuePair<string,SoftComponent>>();
            foreach (KeyValuePair<string, SoftComponent> kvp in mListCompShortCut)
            {
                if (kvp.Value != comp)
                    newList.Add(kvp);

            }
            mListCompShortCut = newList;
            comp.ejected();
            return true;
        }

        public int removeComponent(Type className)
        {
            List<SoftComponent> temp = new System.Collections.Generic.List<SoftComponent>();

            int removed = 0;
            foreach (SoftComponent softComp in mListComponents)
            {
                if (softComp.GetType() != className)
                    temp.Add(softComp);
                else
                {
                    removed++;
                    softComp.ejected();
                }
            }
            mListComponents = temp;

            {

                List<KeyValuePair<string, SoftComponent>> newList = new List<KeyValuePair<string, SoftComponent>>();
                foreach (KeyValuePair<string, SoftComponent> kvp in mListCompShortCut)
                {
                    if (kvp.Value.GetType() != className)
                        newList.Add(kvp);

                }
                mListCompShortCut = newList;
            }
            return removed;
        }

        public SoftComponent getComponent(Type className)
        {
            foreach (SoftComponent softComp in mListComponents)
            {
                if (softComp.GetType() == className)
                    return softComp;
            }

            return null;
        }

        internal SoftComponent getComponent(Type typeName, string shortcut)
        {
            foreach (KeyValuePair<string, SoftComponent> kvp in mListCompShortCut)
            {
                if (kvp.Key.Contains(shortcut) && kvp.Value.GetType() == typeName)
                {
                    return kvp.Value;
                }
            }
            return null;
        }

        public int getComponent(Type className, System.Collections.Generic.ICollection<SoftComponent> container)
        {
            foreach (SoftComponent softComp in mListComponents)
            {
                if (softComp.GetType() == className)
                    container.Add(softComp);
            }

            return container.Count;
        }

        public bool referenceComponent(SoftComponent component)
        {
            return !SoftComponent.ReferenceEquals(getComponent(component.GetType()), null);
        }

        protected ICollection<SoftComponent> components
        {
            get { return mListComponents; }
        }
        #endregion
    }
}
