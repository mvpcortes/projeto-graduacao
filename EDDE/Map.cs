using System;
using System.Collections.Generic;
using System.Text;
using util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using edde.objects;

namespace edde
{
    /// <summary>
    /// Implementa um Mapa 2D para ser pecorrido durante o jogo.
    /// <remarks>
    /// Este mapa possui v�rias propriedades especiais, entre elas as v�rias camadas, conter GameObjects e SoftComponents, entre outras.
    /// </remarks>
    /// </summary>
    public class Map: Scene
    {
        /// <summary>
        /// Enumera��o que especifica as camadas de tileset exibidas no mapa
        /// </summary>
        public enum LAYER
        {
            NONE     = -1,   //its devil! ho ho ho! N�o grava nada
            SUBSOLO  =  0,   //abaixo do solo, como buracos, escadas, etc...
            SOLO,            //posi��o do solo. O ch�o.
            ROOM,            //posi��o dos personagens. Normalmente nesta posi��o h� a colis�o
            UP_ROOM,         //posi��o de cima dentro de um c�modo. Como o topo de arm�rios
            ROOF,            //telhado, tudo que est� no telhado
            LENGTH           //
        }

        private  int ZOrderComparison(GameObject a, GameObject b)
        {
            return MyMath.round(a.getPosition().Y - b.getPosition().Y);
        }

        /// <summary>
        /// Gerenciador de colis�o. Isto servir� principalmente para implementrat
        /// </summary>
        private ICollisionManager mCollisionManager;
    	 
        /// <summary>
        /// A c�mera que especifica a janela de visualiza��o
        /// </summary>
	    private Camera mCamera;

        /// <summary>
        /// Um tileset usado pelo Mapa
        /// </summary>    	  
	    private TileSet mTileSet;

        /// <summary>
        /// Cubo contendo as camadas de mapas e quais tilesets eles representam.
        /// </summary>
        private Matrix<int>[] mLayerMatrix;

        /// <summary>
        /// A largura do mapa em TileSets
        /// </summary>
        private int mWidth;

        /// <summary>
        /// A largura do mapa em TileSets
        /// </summary>
        private int mHeight;


        private List<Wall> mListWalls;

        /// <summary>
        /// Walls que especificam as paredes do map
        /// </summary>
        private List<Wall> mVecRoomWall;

       // private Texture2D mA;
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="rPG">O RPG que possui o mapa</param>
        /// <param name="name">O nome do mapa a ser criado</param>
        /// <param name="name">O tileset a ser usado</param>
        public Map(RPG rPG, string name, string tileset):base(rPG, name)
        {
          //  mA = Resource.getTexture2DFirstColorTransparent("tileset\\audia");
            mVecRoomWall    = new List<Wall>(4);

            mListWalls      = new List<Wall>();
            mLayerMatrix    = new Matrix<int>[(int)LAYER.LENGTH];
            mTileSet        = new TileSet(tileset);
            mWidth          = RPG.me().screenWidth() / mTileSet.getTileWidth();
            mHeight         = RPG.me().screenHeight() / mTileSet.getTileHeight();
            for (int i = 0; i < mLayerMatrix.Length; i++)
                mLayerMatrix[i] = new Matrix<int>(mWidth, mHeight, TileSet.INVALID_TILESET);


            //mCollisionManager = new SimpleCollision(this.getGameObjects(), this.getWalls());
            mCollisionManager = new CellCollisionManager(this.getWidth(), this.getHeight());//este deve ser depois do TileSet
           // mCollisionManager = new QuadTreeNode(this.getWidth(), this.getHeight());//este deve ser depois do TileSet
            mCamera           = new Camera(this);
        }

        public Map(RPG rPG, string name)
            : this(rPG, name, name)
        {}

        public void setTileSetSize(int w, int h)
        {
            foreach (Matrix<int> mat in mLayerMatrix)
            {
                mat.setSize(w, h, TileSet.INVALID_TILESET);
            }
            mWidth = w;
            mHeight = h;

            mCollisionManager.setSizeRegion(getWidth(), getHeight());

            foreach (Wall wall in mVecRoomWall)
            {
                mCollisionManager.removeObject(wall);
                mListWalls.Remove(wall);
            }

            mVecRoomWall.Clear();
            RectFloat temp = new RectFloat(0, 0, getWidth(), getHeight());
            //temp.expandFromScale(-0.2f);
            Wall.insertRoomWalls(mVecRoomWall, temp );

            foreach (Wall wall in mVecRoomWall)
            {
                mCollisionManager.addObject(wall);
                mListWalls.Add(wall);
            }
        }

        public void importStringData(LAYER layer, string data)
        {
            string [] lines = data.Split('\n');
            int y = 0;
            int nLayer = (int)layer;
            foreach (string str in lines)
            {
                if (y >= getTileHeight()) break;
                string[] param = str.Split(',');
                int x = 0;
                foreach (string value in param)
                {
                    if (x >= getTileWidth()) break;
                    try
                    {
                        mLayerMatrix[nLayer][x, y] = Int32.Parse(value);
                    }
                    catch (FormatException fe)
                    {
                        mLayerMatrix[nLayer][x, y] = TileSet.INVALID_TILESET;
                    }

                    x++;
                }
                y++;
            }
        }


        /// <summary>
        /// Retona a largura do mapa.
        /// </summary>
        /// <returns></returns>
        public int getWidth()
        {
            return mWidth*mTileSet.getTileWidth();
        }

        /// <summary>
        /// Retorna a altura do mapa.
        /// </summary>
        /// <returns></returns>
        public int getHeight()
        {
            return mHeight*mTileSet.getTileHeight();
        }

        /// <summary>
        /// Retorna a largura em n� de tilesets
        /// </summary>
        /// <returns></returns>
        public int getTileWidth()
        {
            return mWidth;
        }

        /// <summary>
        /// Retorna a altura do mapa em n� de tilesets
        /// </summary>
        /// <returns></returns>
        public int getTileHeight()
        {
            return mHeight;
        }


        #region Screne Members
        private static IList<ICollised> msListCollisionGameObject = new List<ICollised>();//s�o listas array pq haver� pouca modificia��o do conte�do dos gameObjects
        private static IList<ICollised> msListTouchGameObject = new List<ICollised>();
       /* private static IList<Wall> msListCollisionWall = new List<Wall>();
        private static IList<GameObject> msListNewPosGameObject = new List<GameObject>();*/
         
        public override void Update(GameTime gameTime)
        {
            foreach (SoftComponent sc in mListComponents)
                sc.update(gameTime);

            /*
            foreach (GameObject go in getGameObjects())
            {
                mCollisionManager.removeObject(go);
                go.update(gameTime);
                mCollisionManager.addObject(go);
            }*/

            mCollisionManager.preTest();

            foreach (GameObject go in getGameObjects())
            {
                mCollisionManager.removeObject(go);
                go.update(gameTime);
                mCollisionManager.addObject(go);

                if (go is Hero)
                {
                    int a = 01;
                }
                mCollisionManager.testColision(go, msListCollisionGameObject, msListTouchGameObject);
                if (msListCollisionGameObject.Count > 0)
                {
                    if (go is Hero)
                    {
                        int a = 01;
                    }
                    if (go.onColision(msListCollisionGameObject))
                    {
                        mCollisionManager.removeObject(go);
                        go.restorePrevPosition();
                        mCollisionManager.addObject(go);
                    }
                }
                else
                {
                    go.backupPosition();
                    go.onTouch(msListTouchGameObject);
                }
                msListCollisionGameObject.Clear();
                msListTouchGameObject.Clear();
            }

            msListCollisionGameObject.Clear();
            msListTouchGameObject.Clear();
            
            //atualiza a ordem do "zbuffer"
            getGameObjects().Sort(ZOrderComparison);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch sb = RPG.me().getSpriteBatch();

            //descobre o offset
            Rectangle rect = mCamera.processRect();
            Point initCell, endCell;

            {
                initCell.X = Math.Max(0, rect.Left / getTileWidth());
                initCell.Y = Math.Max(0, rect.Top / getTileHeight());

                endCell.X  = Math.Min(getTileWidth()-1 , (rect.Right  /  mTileSet.getTileWidth() ) + 1);
                endCell.Y  = Math.Min(getTileHeight()-1, (rect.Bottom /  mTileSet.getTileHeight()) + 1);
            }

            Matrix<int> matLayer = mLayerMatrix[(int)LAYER.SOLO];
            for (int i = initCell.X; i <= endCell.X; i++)
                for (int j = initCell.Y; j <= endCell.Y; j++)
                {
                    if (matLayer[i, j] != TileSet.INVALID_TILESET)
                    {

                        Rectangle rectDest = new Rectangle(
                            i * mTileSet.getTileWidth() - rect.Left,
                            j * mTileSet.getTileHeight() - rect.Top,
                            mTileSet.getTileWidth(),
                            mTileSet.getTileHeight()
                            );

                        Rectangle rectSource = mTileSet.getRect(matLayer[i, j]);

                        sb.Draw(mTileSet.getTexture(), rectDest, rectSource, Color.White);
                    }
                }
            List<GameObject> listTemp = getGameObjects();
        
            RectFloat rectFloat = RectFloat.FromRectangle(rect);
            //rectFloat.expandFromScale(-0.3f);
            foreach(GameObject go in getGameObjects())
            {
               go.draw(gameTime, new Point(rect.Left, rect.Top));
            }

           // Texture2D texture = Resource.getTexture2DFirstColorTransparent("tileset\\audia");
            foreach (Wall wall in getWalls())
            {
                Rectangle rectWall = wall.getRectBase().toRectangle();
                rectWall.X -= rect.Left;
                rectWall.Y -= rect.Top;
                sb.Draw(mTileSet.getTexture(), rectWall, mTileSet.getRect(10), Color.White);

            }

            foreach (SoftComponent sc in mListComponents)
                sc.draw(gameTime, new Point(rect.Left, rect.Top));

        }

        public override void addObject(GameObject obj)
        {
            base.addObject(obj);
            mCollisionManager.addObject(obj);
        }
        #endregion

        public void addWall(Wall wall)
        {
            mListWalls.Add(wall);
            mCollisionManager.addObject(wall);
        }

        internal ICollection<Wall> getWalls()
        {
            return this.mListWalls;
        }

        public void followObject(GameObject obj)
        {
            mCamera.followIt(obj);
        }

       public void setCameraRect(Rectangle rect)
       {
           mCamera.setVirtualRect(rect);
       }
    }
}