using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using edde.components;

namespace edde
{
    /*
    /// <summary>
    /// classe que trabalha como m�quina de estados e gera padr�es de mensagem de acordo com um estado configurado
    /// </summary>
    static class MessageFactory
    {
        public static struct TimeChar
        {
            public TimeSpan SLOW    = new TimeSpan(0,0,0,0,500);
            public TimeSpan NORMAL  = new TimeSpan(0,0,0,0,50);
            public TimeSpan FAST    = new TimeSpan(0,0,0,0,10);
        };

        public static struct TimeShow
        {
            public TimeSpan SLOW    = new TimeSpan(0,0,0,3);
            public TimeSpan NORMAL  = new TimeSpan(0,0,0,1);
            public TimeSpan FAST    = new TimeSpan(0,0,0,0,500);
        };

        enum ScreenDestiny
        { 
            NONE,
            TOP,
            MIDDLE,
            BOTTOM,
            AUTO_HERO
        }

        SpriteFont mFont    = null;
        Texture2D mFrame    = null;
        Color mColor        = Color.White;
        TimeSpan mTimeChar  = TimeChar.NORMAL;
        TimeSpan mTimeShow  = TimeShow.NORMAL;//tempo de exibi��o da janela

        Vector2 mDestiny = Vector2.Zero;
        ScreenDestiny mScreenDestiny = ScreenDestiny.NONE;

        Vector2 mSource = Vector2.Zero;
        GameObject mSourceGameObject = null;
        public void setFont(string fontName)
        {
            mTexture = Resource.getFont(fontName);
        }

        public void setFrame(string frame)
        {
            mFrame = Resource.getTexture2DFirstColorTransparent(Resource.PATH_FRAME + frame);
        }

        public void setColor(Color color){       mColor = color;      }

        public void setTimeText(TimeSpan time){     mTimeText = time;     }

        public void setTimeShow(TimeSpan time){     mTimeShow = time;     }

        public void setDestiny(Vector2 pos)
        {
            mDestiny = pos;
            mScreenDestiny = ScreenDestiny.NONE;
        }

        public void setScreenDestiny(ScreenDestiny sd)
        {
            mDestiny = Vector2.Zero;
            mScreenDestiny = sd;
        }

        public void setSource(Vector2 vec)
        {
        }

        Message createMessage(string msg, Vector2 source, Vector2 destiny);
   
    }  */
}
