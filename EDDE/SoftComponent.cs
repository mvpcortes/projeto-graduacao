using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using System.Reflection;

namespace edde
{

    /// <summary>
    /// Classe que representa uma inje��o no c�digo de Componentes (anota��o sobre o componente que permite que ele seja criado automaticamente.
    /// </summary>
    public class Injection : System.Attribute 
    {
        private string mStrDiferential = "";

        public string diferenctial
        {
            get{return mStrDiferential;}
        }

        public Injection()
        {
        }

        public Injection(string strDif)
        {
            mStrDiferential = strDif;
        }
    }

    /// <summary>
    /// Classe que representa um Componente dentro da Arquitetura do GameObject
    /// </summary>
    public interface SoftComponent
    {
        
        /// <summary>
        /// Faz a impress�o na tela
        /// </summary>
        /// <param name="time">o tempo do jogo</param>
        /// <param name="offSet">offSet de impress�o adotado pelo pai</param>
        void draw(GameTime time, ref Rectangle rectWindow);

        /// <summary>
        /// Atualiza as informa��es do componente
        /// </summary>
        /// <param name="time">O Tempo corrente do jogo</param>
        void update(GameTime time);

        /// <summary>
        /// Inicia o componente. Normalmente � necess�rio usar o parent para isso, pois precisamos acessar outros componentes contidos nele.
        /// </summary>
        /// <param name="parent">Refer�ncia ao objeto que cont�m este componente</param>
        /// <returns>retorna se foi poss�vel iniciar satisfatoriamente o componente</returns>
        bool init(GameObject myParent);
        /*{
            bool ok = true;
            FieldInfo[] vecMemberInfo = GetType().GetFields(BindingFlags.GetField | BindingFlags.Instance);
            foreach (FieldInfo mi in vecMemberInfo)
            {
                object[] vecObjInject = mi.GetCustomAttributes(typeof(edde.physicalmanager.Injection), true);
                if (vecObjInject.Length > 0)
                {
                    SoftComponent sc = parent.getComponent(mi.FieldType);
                    if (sc)
                    {
                        mi.SetValue(this, sc);
                    }
                    else
                    {
                        ok = false;
                    }
                    
                    Object obj = mi.GetValue(this);
                    SoftComponent component = obj as SoftComponent;
                    if (!ReferenceEquals(component, null))
                        addComponent(component);
                     
                }
            }
            return ok;
        }*/

        /// <summary>
        /// Avisa ao componente que ele foi retirado do seu container.
        /// </summary>
        void ejected();


        /// <summary>
        /// Retorna se o componente quer ser ejetado.
        /// </summary>
        /// <remarks>
        /// Usado para fazer a destrui��o de um componente
        /// </remarks>
        /// <returns></returns>
        bool ejectMe();

        /// <summary>
        /// Verifica se um componente est� habilitado ou n�o (passa no ciclo update e draw).
        /// </summary>
        bool enable{get;}
    }
}
