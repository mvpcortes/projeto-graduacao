﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace util
{
    public static class StrTools
    {
        private static CultureInfo ms_ciEUA = CultureInfo.GetCultureInfo("en-US");
        private static CultureInfo ms_ciBRA = CultureInfo.GetCultureInfo("pt-BR");

        public static float toFloat(string str)
        {
            if (ReferenceEquals(str, null) || str == "") return 0f;
            float f = 0;
            //verfica max e min
            {
                if (str.IndexOf("max") >= 0)
                {
                    if (str.IndexOf('-') >= 0)
                        return -1 * float.MaxValue;
                    else
                        return float.MaxValue;
                }

                if (str.IndexOf("min") >= 0)
                {
                    if (str.IndexOf('-') >= 0)
                        return -1 * float.MinValue;
                    else
                        return float.MinValue;
                }
            }

            if (!float.TryParse(str, System.Globalization.NumberStyles.Float, ms_ciEUA.NumberFormat, out f))
                if (!float.TryParse(str, System.Globalization.NumberStyles.Float, ms_ciBRA.NumberFormat, out f))
                    throw new FormatException("A string não obdece um formato de número float válido.");

            return f;
        }

        public static float[] toVecFloat(string value)
        {
            return toVecFloat(value, ',');
        }
        public static float[] toVecFloat(string value, char separator)
        {
            int first = value.IndexOf("(");
                if (first < 0)
                    first = value.IndexOf("{");

                int last = value.LastIndexOf(")");
                if (last < 0)
                    last = value.LastIndexOf("}");

                if( first >= 0 && last >= 0 && first < last)
                {
                    value = value.Substring(first+1, last - first -1);
                }
                
                string[] temp = value.Split(separator);
                float[] tempFloat = new float[temp.Length];
                for(int i = 0; i < tempFloat.Length;i++)
                    tempFloat[i] = toFloat(temp[i]);

                return tempFloat;
        }
    }
}
