using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace util
{
    public struct SimpleComparer<T> : IComparer<T> where T : IComparable<T>
    {

        #region IComparer<T> Members

        public int Compare(T x, T y)
        {
            return x.CompareTo(y);
        }

        #endregion
    }


    public class PriorityList<T> : ICollection<T>, IEnumerable<T> where T : IComparable<T> 
    {
        /// <summary>
        /// Delegate para visitar os itens
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public delegate int visitMethod(T item);

        private LinkedList<T> mLinkedList;
        private IComparer<T> mComparer;


        public void setComparer(IComparer<T> comparer) { mComparer = comparer; }

        public PriorityList()
        {
            mLinkedList = new LinkedList<T>();
            mComparer = new SimpleComparer<T>();
        }

        public PriorityList(IEnumerable<T> coll)
            : this()
        {
            foreach (T t in coll)
                Add(t);
        }

        public PriorityList(IComparer<T> comparer)
        {
            mLinkedList = new LinkedList<T>();
            mComparer = comparer;
        }

        public PriorityList(IComparer<T> comparer, IEnumerable<T> coll)
        {
            mComparer = comparer;
            foreach (T t in coll)
                Add(t);
        }

        public T PopFirst()
        {
            T t = mLinkedList.First.Value;
            mLinkedList.RemoveFirst();
            return t;
        }

        public T First()
        {
            return mLinkedList.First.Value;
        }

        public override string ToString()
        {
            string temp = "";
            foreach (T t in mLinkedList)
            {
                temp += t.ToString() + ", ";

            }

            return temp;
        }
        #region ICollection<T> Members

        public virtual void Add(T item)
        {
            //direct find
            LinkedListNode<T> node = mLinkedList.First;

            if (ReferenceEquals(node, null))
            {
                mLinkedList.AddFirst(item);
            }
            else
            {
                //pr� testes
                if (mComparer.Compare(mLinkedList.Last.Value, item) <= 0)
                {
                    mLinkedList.AddLast(item);
                    return;
                }
                else if (mComparer.Compare(mLinkedList.First.Value, item) >= 0)
                {
                    mLinkedList.AddFirst(item);
                    return;
                }

                while (!ReferenceEquals(node, null))
                {
                    if (mComparer.Compare(node.Value, item) <= 0)
                        node = node.Next;
                    else
                        break;
                }

                if (ReferenceEquals(node, null))
                    mLinkedList.AddLast(item);
                else
                {
                    mLinkedList.AddBefore(node, new LinkedListNode<T>(item));
                }
            }
        }

        public void Clear()
        {
            mLinkedList.Clear();
        }

        public bool Contains(T item)
        {
            return mLinkedList.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            mLinkedList.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.mLinkedList.Count; }
        }

        public bool Remove(T item)
        {
            return mLinkedList.Remove(item);
        }

        public bool IsReadOnly
        {
            get { return false; }
        }


        #endregion

        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return mLinkedList.GetEnumerator();
        }

        #endregion
       

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return mLinkedList.GetEnumerator();
        }

        #endregion

        public void CutLastUntilRest(int p)
        {
            p = Math.Max(p, 0);
            while (mLinkedList.Count > p)
            {
                mLinkedList.RemoveLast();
            }
        }

        public void removeItensWith(visitMethod method)
        {
            LinkedList<T> newList = new LinkedList<T>();

            foreach(T item in newList)
            {
                if(method(item) != 0)
                    newList.AddLast(item);
            }

            mLinkedList = newList;

        }
    }
}
